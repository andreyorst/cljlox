;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((zig-mode . ((imenu-auto-rescan . t)
              (eval . (save-excursion
                        (goto-char (point-min))
                        (and hs-minor-mode
                             (search-forward "fn makeRules() []ParseRule {" nil t)
                             (hs-hide-block))))
              (eval . (setq-local tags-file-name
                                  (expand-file-name "TAGS" (project-root (project-current)))))
              (after-save-hook . (lambda nil (require 'project)
                                   (let ((default-directory (project-root (project-current))))
                                     (make-process :name "zig-tags" :buffer " *zig-tags*" :command
                                                   '("make" "tags") :connection-type 'pipe)))))))
