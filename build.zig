const std = @import("std");
const Build = std.Build;
const Step = Build.Step;
const CompStep = Step.Compile;
const Mode = std.builtin.Mode;
const CrossTarget = std.zig.CrossTarget;
const trimRight = std.mem.trimRight;
const parseInt = std.fmt.parseInt;
const exit = std.process.exit;
const fs = std.fs;
const str = []const u8;

const MAJOR = 0;
const MINOR = 1;

const Version = struct {
    major: u32,
    minor: u32,
    commits: u32,
};

/// Get the revision count from Git
fn gitCountRevs(b: *Build) !u32 {
    var code: u8 = undefined;
    const build_root = b.build_root.path orelse return error.NoBuildRootPath;
    const rev_count = try b.execAllowFail(&.{ "git", "-C", build_root, "rev-list", "HEAD", "--count" }, &code, .Ignore);
    const commits_num = trimRight(u8, rev_count, "\n");
    return try parseInt(u32, commits_num, 10);
}

fn getVersion(b: *Build) Version {
    return Version{
        .major = MAJOR,
        .minor = MINOR,
        .commits = gitCountRevs(b) catch exit(1),
    };
}

fn isZigSrc(name: str) bool {
    return name.len > 3 and std.mem.eql(u8, name[name.len - 4 ..], ".zig");
}

/// Automatically finds tests under the test directory, adds the test
/// to the build `step`, and adds all necessary modules for test to
/// run.
fn addTestStep(b: *Build, optimize: Mode, target: CrossTarget) void {
    const test_step = b.step("test", "Run unit tests");
    const test_dir = b.pathJoin(&.{ "test", "zig", "lox" });
    const dir = std.fs.cwd().openIterableDir(test_dir, .{}) catch exit(2);
    var iterator = dir.iterate();

    while (iterator.next() catch null) |path| {
        if (isZigSrc(path.name)) {
            const tests = b.addTest(.{
                .name = path.name,
                .root_source_file = .{ .path = b.pathJoin(&.{ test_dir, path.name }) },
                .target = target,
                .optimize = optimize,
            });
            test_step.dependOn(&b.addRunArtifact(tests).step);
        }
    }
}

pub fn build(b: *Build) void {
    const version = b.fmt("{[major]d}.{[minor]d}.{[commits]d}", getVersion(b));

    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    const main = b.pathJoin(&.{ "src", "zig", "lox", "main.zig" });

    const exe = b.addExecutable(.{
        .name = b.fmt("lox-{s}", .{version}),
        .root_source_file = .{ .path = main },
        .target = target,
        .optimize = optimize,
    });

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    addTestStep(b, optimize, target);
}
