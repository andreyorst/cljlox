# (Zig|Clj)Lox - Lox interpreter in Clojure and Zig

Lox is a language developed through the [craftinginterpreters.com][1] book.
This repository contains a tree walking interpreter written in Clojure and a bytecode VM written in Zig.

## Clojure
Because of Clojure's focus on immutability and avoidance of OOP, some changes had to be made to the interpreter code structure.

Instead of the [Visitor pattern][2], as used in the book, this implementation relies on [protocols][3] and everything is immutable where it is not too out of hand.
The only mutable part of the interpreter is the runtime environment, which is implemented as a Clojure's `volatile`.
The parser, resolver, and tokenizer are fully immutable.

### Building and running

Execute `make cljlox` in the root directory of the repository.
Run with `make run_cljlox`.

To run tests, use `make test_cljlox`.

## Zig

The book implements VM in C, thus some changes had to be made to write the same code in Zig.
I've tried to make as few changes as possible, but some concepts, like dynamic arrays are already provided by the language, so where possible I've used existing implementations from the standard library.

### Building and running

This language was built with Zig 0.11.0.
Due to frequent changes of Zig's build API with each major release, this project may not build with newer versions.

Execute `make release_ziglox` in the root directory of the repository.
Run with `make run_ziglox`.

To run tests, use `make test_ziglox`.

## Zig (beyond the book)

I liked hacking on this project in Zig quite a lot, and decided to tweak the language a bit further.
The canonical version of Lox can be found at commit 028d480054fa01df3c29beadd3462172e3cd29aa.

Differences from the book:

- Removed classes;
- Added collections:
  - Arrays: `[1, 2, "foo", car]`
  - Hash tables: `#{.answer => 42, "my other car is a" => cdr}`;
  - Hash sets: `#[1, 2, "foo", rplacadabra]`
- Added support for escaping in strings;
- Replaced `print` statement with `print` function;
- Added anonymous functions: `var inc = fun (x) { return x + 1; }`;
- Added short lambda expressions: `var add = \x, y -> x + y;`
- Added `tostring`, `length`, and `sleep` functions,
- Added indexing syntax, for non-compile-time field access: `foo["bar"]`.
  - The same syntax is used by arrays, except only accepts integers: `baz[1]`.
- Implemented a pretty-printer for all supported data structures.

## License

[MIT](LICENSE)

[1]: https://craftinginterpreters.com
[2]: https://en.wikipedia.org/wiki/Visitor_pattern
[3]: https://clojure.org/reference/protocols

<!--  LocalWords:  bytecode VM Zig runtime tokenizer OOP LocalWords
      LocalWords:  craftinginterpreters
 -->
