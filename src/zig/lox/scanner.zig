const std = @import("std");

const common = @import("common.zig");
const str = common.str;

pub const TokenType = enum {
    // Single-character tokens.
    LEFT_PAREN,
    RIGHT_PAREN,
    LEFT_BRACE,
    RIGHT_BRACE,
    LEFT_BRACKET,
    RIGHT_BRACKET,
    COMMA,
    DOT,
    MINUS,
    PLUS,
    SEMICOLON,
    COLON,
    SLASH,
    STAR,
    HASH,
    LAMBDA,
    // One or two character tokens.
    BANG,
    BANG_EQUAL,
    EQUAL,
    EQUAL_EQUAL,
    GREATER,
    GREATER_EQUAL,
    LESS,
    LESS_EQUAL,
    ARROW,
    FAT_ARROW,
    // Literals.
    IDENTIFIER,
    STRING,
    NUMBER,
    // Keywords.
    AND,
    ELSE,
    FALSE,
    FOR,
    FUN,
    IF,
    NIL,
    OR,
    RETURN,
    TRUE,
    VAR,
    WHILE,
    // Rest
    ERROR,
    EOF,
};

pub const Token = struct {
    Type: TokenType,
    line: u32,
    name: str,
};

pub const Scanner = struct {
    const Self = @This();
    source: *const str,
    current: u32,
    start: u32,
    line: u32,

    pub fn init(source: *const str) Self {
        return Self{
            .start = 0,
            .current = 0,
            .line = 1,
            .source = source,
        };
    }

    pub fn deinit(_: *Self) void {}

    fn makeToken(self: *Self, Type: TokenType) Token {
        return Token{
            .Type = Type,
            .name = self.source.*[self.start .. self.start + (self.current - self.start)],
            .line = self.line,
        };
    }

    fn makeErrorToken(self: *Self, message: str) Token {
        return Token{
            .Type = .ERROR,
            .name = message,
            .line = self.line,
        };
    }

    fn isAtEnd(self: *Self) bool {
        return self.current == self.source.len;
    }

    fn advance(self: *Self) u8 {
        self.current += 1;
        return self.source.*[self.current - 1];
    }

    fn match(self: *Self, expected: u8) bool {
        if (self.isAtEnd()) return false;
        if (self.source.*[self.current] != expected) return false;
        self.current += 1;
        return true;
    }

    inline fn peek(self: *Self) u8 {
        if (self.isAtEnd()) return 0;
        return self.source.*[self.current];
    }

    inline fn peekNext(self: *Self) u8 {
        if (self.isAtEnd()) return 0;
        if (self.current + 1 == self.source.len) return 0;
        return self.source.*[self.current + 1];
    }

    fn skipWhitespace(self: *Self) void {
        while (true) {
            switch (self.peek()) {
                ' ', '\r', '\t' => _ = self.advance(),
                '\n' => {
                    _ = self.advance();
                    self.line += 1;
                },
                '/' => if (self.peekNext() == '/') {
                    while (self.peek() != '\n' and !self.isAtEnd())
                        _ = self.advance();
                } else {
                    return;
                },
                else => return,
            }
        }
    }

    fn string(self: *Self) Token {
        var escaped = false;
        while (!self.isAtEnd()) {
            switch (self.peek()) {
                '\n' => {
                    self.line += 1;
                    escaped = false;
                },
                '\\' => escaped = !escaped,
                '"' => if (!escaped) {
                    break;
                } else {
                    escaped = false;
                },
                else => escaped = false,
            }
            _ = self.advance();
        }
        if (self.isAtEnd()) return self.makeErrorToken("Unterminated string.");
        _ = self.advance();
        return self.makeToken(.STRING);
    }

    fn isDigit(c: u8) bool {
        return c >= '0' and c <= '9';
    }

    fn number(self: *Self) Token {
        while (isDigit(self.peek()))
            _ = self.advance();

        if (self.peek() == '.' and isDigit(self.peekNext())) {
            _ = self.advance();
            while (isDigit(self.peek())) _ = self.advance();
        }

        return self.makeToken(.NUMBER);
    }

    fn isAlpha(c: u8) bool {
        return (c >= 'a' and c <= 'z') or
            (c >= 'A' and c <= 'Z') or
            c == '_';
    }

    fn checkKeyword(
        self: *Self,
        start: u32,
        length: u32,
        rest: str,
        Type: TokenType,
    ) TokenType {
        if ((self.current - self.start == start + length) and
            std.mem.eql(u8, rest, self.source.*[(self.start + start)..(self.start + start + length)]))
            return Type;

        return .IDENTIFIER;
    }

    fn identifierType(self: *Self) TokenType {
        switch (self.source.*[self.start]) {
            'a' => return self.checkKeyword(1, 2, "nd", .AND),
            'e' => return self.checkKeyword(1, 3, "lse", .ELSE),
            'f' => if (self.current - self.start > 1)
                switch (self.source.*[self.start + 1]) {
                    'a' => return self.checkKeyword(2, 3, "lse", .FALSE),
                    'o' => return self.checkKeyword(2, 1, "r", .FOR),
                    'u' => return self.checkKeyword(2, 1, "n", .FUN),
                    else => return .IDENTIFIER,
                },
            'i' => return self.checkKeyword(1, 1, "f", .IF),
            'n' => return self.checkKeyword(1, 2, "il", .NIL),
            'o' => return self.checkKeyword(1, 1, "r", .OR),
            'r' => return self.checkKeyword(1, 5, "eturn", .RETURN),
            't' => if (self.current - self.start > 1)
                switch (self.source.*[self.start + 1]) {
                    'r' => return self.checkKeyword(2, 2, "ue", .TRUE),
                    else => return .IDENTIFIER,
                },
            'v' => return self.checkKeyword(1, 2, "ar", .VAR),
            'w' => return self.checkKeyword(1, 4, "hile", .WHILE),
            else => return .IDENTIFIER,
        }
        return .IDENTIFIER;
    }

    fn identifier(self: *Self) Token {
        while (isAlpha(self.peek()) or isDigit(self.peek()))
            _ = self.advance();
        return self.makeToken(self.identifierType());
    }

    pub fn scan(self: *Self) Token {
        self.skipWhitespace();
        self.start = self.current;
        if (self.isAtEnd())
            return self.makeToken(.EOF);

        const c = self.advance();

        if (isDigit(c)) {
            return self.number();
        } else if (isAlpha(c)) {
            return self.identifier();
        }

        switch (c) {
            '(' => return self.makeToken(.LEFT_PAREN),
            ')' => return self.makeToken(.RIGHT_PAREN),
            '[' => return self.makeToken(.LEFT_BRACKET),
            ']' => return self.makeToken(.RIGHT_BRACKET),
            '{' => return self.makeToken(.LEFT_BRACE),
            '}' => return self.makeToken(.RIGHT_BRACE),
            ';' => return self.makeToken(.SEMICOLON),
            ':' => return self.makeToken(.COLON),
            ',' => return self.makeToken(.COMMA),
            '.' => return self.makeToken(.DOT),
            '-' => return self.makeToken(if (self.match('>')) .ARROW else .MINUS),
            '+' => return self.makeToken(.PLUS),
            '/' => return self.makeToken(.SLASH),
            '\\' => return self.makeToken(.LAMBDA),
            '*' => return self.makeToken(.STAR),
            '!' => return self.makeToken(if (self.match('=')) .BANG_EQUAL else .BANG),
            '=' => return self.makeToken(if (self.match('=')) .EQUAL_EQUAL else if (self.match('>')) .FAT_ARROW else .EQUAL),
            '<' => return self.makeToken(if (self.match('=')) .LESS_EQUAL else .LESS),
            '>' => return self.makeToken(if (self.match('=')) .GREATER_EQUAL else .GREATER),
            '"' => return self.string(),
            '#' => return self.makeToken(.HASH),
            else => return self.makeErrorToken("unsupported character"),
        }
    }
};
