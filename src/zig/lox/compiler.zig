const std = @import("std");
const parseFloat = std.fmt.parseFloat;
const ArrayList = std.ArrayList;

const common = @import("common.zig");
const eprint = common.eprint;
const str = common.str;
const OPCode = common.OPCode;
const Code = common.Code;
const CallFrame = common.CallFrame;
const LoxError = common.LoxError;

const Value = @import("value.zig").Value;

const Chunk = @import("chunk.zig").Chunk;

const scan = @import("scanner.zig");
const Scanner = scan.Scanner;
const Token = scan.Token;
const TokenType = scan.TokenType;

const VM = @import("vm.zig").VM;
const Object = @import("object.zig");
const String = Object.String;
const Function = Object.Function;
const Array = Object.Array;
const HashTable = Object.HashTable;

const DEBUG_PRINT_CODE = false;

const Upvalue = struct {
    index: u8,
    isLocal: bool,
};

const Local = struct {
    name: Token,
    depth: i32,
    isCaptured: bool,
};

const FunctionType = enum {
    Function,
    Script,
    Anonymous,
    Lambda,
    Method,
};

const MAX_LOCALS = std.math.maxInt(u8) + 1;
const MAX_UPVALUES = std.math.maxInt(u8) + 1;

pub const Compiler = struct {
    enclosing: ?*Compiler,
    function: *Function,
    Type: FunctionType,
    locals: ArrayList(Local),
    upvalues: ArrayList(Upvalue),
    scopeDepth: u32,

    fn init(vm: *VM, Type: FunctionType, enclosing: ?*Compiler) LoxError!Compiler {
        var locals = ArrayList(Local).init(vm.allocator);
        locals.append(Local{
            .depth = 0,
            .name = Token{
                .name = if (Type == .Function) "" else "this",
                .Type = .IDENTIFIER,
                .line = 1,
            },
            .isCaptured = false,
        }) catch unreachable;
        return Compiler{
            .enclosing = enclosing,
            .Type = Type,
            .function = try Function.new(vm),
            .locals = locals,
            .scopeDepth = 0,
            .upvalues = ArrayList(Upvalue).init(vm.allocator),
        };
    }

    fn deinit(self: *Compiler) void {
        self.locals.deinit();
        self.upvalues.deinit();
    }
};

const Precedence = enum {
    NONE,
    ASSIGNMENT, // =
    OR, // or
    AND, // and
    EQUALITY, // == !=
    COMPARISON, // < > <= >=
    TERM, // + -
    FACTOR, // * /
    UNARY, // ! -
    CALL, // . ()
    PRIMARY,
};

const ParseFn = *const fn (parser: *Parser, can_assign: bool) LoxError!void;

const ParseRule = struct {
    prefix: ?ParseFn,
    infix: ?ParseFn,
    precedence: Precedence,
};

fn makeRules() []ParseRule {
    var r = [_]ParseRule{undefined} ** @typeInfo(TokenType).Enum.fields.len;
    r[@intFromEnum(TokenType.LEFT_PAREN)] = .{ .prefix = Parser.grouping, .infix = Parser.call, .precedence = .CALL };
    r[@intFromEnum(TokenType.RIGHT_PAREN)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.LEFT_BRACE)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.RIGHT_BRACE)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.LEFT_BRACKET)] = .{ .prefix = Parser.array, .infix = Parser.arrayIndex, .precedence = .CALL };
    r[@intFromEnum(TokenType.RIGHT_BRACKET)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.COMMA)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.DOT)] = .{ .prefix = null, .infix = Parser.dot, .precedence = .CALL };
    r[@intFromEnum(TokenType.MINUS)] = .{ .prefix = Parser.unary, .infix = Parser.binary, .precedence = .TERM };
    r[@intFromEnum(TokenType.PLUS)] = .{ .prefix = null, .infix = Parser.binary, .precedence = .TERM };
    r[@intFromEnum(TokenType.SEMICOLON)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.SLASH)] = .{ .prefix = null, .infix = Parser.binary, .precedence = .FACTOR };
    r[@intFromEnum(TokenType.STAR)] = .{ .prefix = null, .infix = Parser.binary, .precedence = .FACTOR };
    r[@intFromEnum(TokenType.BANG)] = .{ .prefix = Parser.unary, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.BANG_EQUAL)] = .{ .prefix = null, .infix = Parser.binary, .precedence = .EQUALITY };
    r[@intFromEnum(TokenType.EQUAL)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.EQUAL_EQUAL)] = .{ .prefix = null, .infix = Parser.binary, .precedence = .EQUALITY };
    r[@intFromEnum(TokenType.GREATER)] = .{ .prefix = null, .infix = Parser.binary, .precedence = .COMPARISON };
    r[@intFromEnum(TokenType.GREATER_EQUAL)] = .{ .prefix = null, .infix = Parser.binary, .precedence = .COMPARISON };
    r[@intFromEnum(TokenType.LESS)] = .{ .prefix = null, .infix = Parser.binary, .precedence = .COMPARISON };
    r[@intFromEnum(TokenType.LESS_EQUAL)] = .{ .prefix = null, .infix = Parser.binary, .precedence = .COMPARISON };
    r[@intFromEnum(TokenType.IDENTIFIER)] = .{ .prefix = Parser.variable, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.STRING)] = .{ .prefix = Parser.string, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.NUMBER)] = .{ .prefix = Parser.number, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.AND)] = .{ .prefix = null, .infix = Parser.and_, .precedence = .AND };
    r[@intFromEnum(TokenType.ELSE)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.FALSE)] = .{ .prefix = Parser.literal, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.FOR)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.FUN)] = .{ .prefix = Parser.anonymousFunction, .infix = null, .precedence = .ASSIGNMENT };
    r[@intFromEnum(TokenType.IF)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.NIL)] = .{ .prefix = Parser.literal, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.OR)] = .{ .prefix = null, .infix = Parser.or_, .precedence = .OR };
    r[@intFromEnum(TokenType.RETURN)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.TRUE)] = .{ .prefix = Parser.literal, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.VAR)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.WHILE)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.ERROR)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.EOF)] = .{ .prefix = null, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.LAMBDA)] = .{ .prefix = Parser.lambda, .infix = null, .precedence = .NONE };
    r[@intFromEnum(TokenType.HASH)] = .{ .prefix = Parser.dispatch, .infix = null, .precedence = .NONE };
    return &r;
}

const rules = makeRules();

fn getRule(t: TokenType) ParseRule {
    return rules[@intFromEnum(t)];
}

pub const Parser = struct {
    current: Token,
    previous: Token,
    scanner: Scanner,
    hadError: bool,
    panicMode: bool,
    compiler: *Compiler,
    vm: *VM,

    fn init(scanner: Scanner, vm: *VM, compiler: *Compiler) Parser {
        return Parser{
            .current = undefined,
            .previous = undefined,
            .hadError = false,
            .panicMode = false,
            .scanner = scanner,
            .vm = vm,
            .compiler = compiler,
        };
    }

    fn deinit(_: *Parser) void {}

    fn errorAt(self: *Parser, token: *Token, message: str) void {
        if (self.panicMode) return;
        self.panicMode = true;
        eprint("[line {d}] Error", .{token.*.line});

        switch (token.Type) {
            .EOF => eprint(" at end", .{}),
            .ERROR => {},
            else => eprint(" at '{s}'", .{token.*.name}),
        }

        eprint(": {s}", .{message});
        self.hadError = true;
    }

    fn error_(self: *Parser, comptime fmt: []const u8, args: anytype) void {
        self.errorAt(&self.previous, "");
        eprint(fmt, args);
        eprint("\n", .{});
    }

    fn errorAtCurrent(self: *Parser, message: str) void {
        if (self.panicMode) return;
        self.errorAt(&self.current, message);
        eprint("\n", .{});
    }

    fn advance(self: *Parser) void {
        self.previous = self.current;

        while (true) {
            self.current = self.scanner.scan();
            if (self.current.Type != .ERROR) break;
            self.errorAtCurrent(self.current.name);
        }
    }

    fn consume(self: *Parser, Type: TokenType, message: str) void {
        if (self.current.Type == Type) return self.advance();
        return self.errorAtCurrent(message);
    }

    fn currentChunk(self: *Parser) *Chunk {
        return &self.compiler.function.chunk;
    }

    fn emitByte(self: *Parser, byte: Code) LoxError!void {
        var chunk = self.currentChunk();
        try chunk.write(byte, self.current.line);
    }

    fn emitBytes(self: *Parser, byte1: Code, byte2: Code) LoxError!void {
        try self.emitByte(byte1);
        try self.emitByte(byte2);
    }

    fn emitReturn(self: *Parser) LoxError!void {
        try self.emitByte(@intFromEnum(OPCode.Nil));
        try self.emitByte(@intFromEnum(OPCode.Ret));
    }

    fn makeConstant(self: *Parser, value: Value) LoxError!Code {
        var chunk = self.currentChunk();
        self.vm.push(value);
        const constant = try chunk.addConstant(value);
        _ = self.vm.pop();
        if (constant > std.math.maxInt(Code)) {
            self.error_("Too many constants in one chunk.", .{});
            return 0;
        }
        return constant;
    }

    fn emitConstant(self: *Parser, value: Value) LoxError!void {
        try self.emitBytes(@intFromEnum(OPCode.Constant), try self.makeConstant(value));
    }

    fn number(self: *Parser, _: bool) LoxError!void {
        const token = self.previous;
        const num = token.name;

        const v = parseFloat(f64, num) catch {
            return self.error_("unable to parse constant '{s}'", .{num});
        };

        try self.emitConstant(Value.from(v));
    }

    fn stringValue(self: *Parser, chars: str) LoxError!Value {
        const s = try String.copy(self.vm, chars[1 .. chars.len - 1]);
        return s.obj.asValue();
    }

    fn string(self: *Parser, _: bool) LoxError!void {
        const s = try self.stringValue(self.previous.name);
        try self.emitConstant(s);
    }

    fn parsePrecedence(self: *Parser, precedence: Precedence) LoxError!void {
        self.advance();
        if (getRule(self.previous.Type).prefix) |prefixRule| {
            const can_assign = @intFromEnum(precedence) <= @intFromEnum(Precedence.ASSIGNMENT);
            try prefixRule(self, can_assign);
            while (@intFromEnum(precedence) <= @intFromEnum(getRule(self.current.Type).precedence)) {
                self.advance();
                if (getRule(self.previous.Type).infix) |infixRule| {
                    try infixRule(self, can_assign);
                }
            }
            if (can_assign and self.match(.EQUAL)) {
                self.error_("Invalid assignment target.", .{});
            }
        } else {
            self.error_("Expect expression.", .{});
        }
    }

    fn identifierConstant(self: *Parser, token: *const Token) LoxError!Code {
        const s = try String.copy(self.vm, token.name);
        return try self.makeConstant(s.obj.asValue());
    }

    fn resolveLocal(self: *Parser, compiler: *Compiler, name: str) ?Code {
        var i = @as(i32, @intCast(compiler.locals.items.len)) - 1;
        while (i >= 0) : (i -= 1) {
            const local = compiler.locals.items[@intCast(i)];
            if (std.mem.eql(u8, name, local.name.name)) {
                if (local.depth == -1) {
                    self.error_("Can't read local variable in its own initializer.", .{});
                }
                return @intCast(i);
            }
        }
        return null;
    }

    fn addUpvalue(self: *Parser, compiler: *Compiler, index: Code, isLocal: bool) ?Code {
        for (compiler.upvalues.items, 0..) |upvalue, i|
            if (upvalue.index == index and upvalue.isLocal == isLocal)
                return @intCast(i);

        if (compiler.upvalues.items.len == MAX_UPVALUES) {
            self.error_("Too many closure variables in function.", .{});
            return null;
        }
        compiler.upvalues.append(Upvalue{
            .isLocal = isLocal,
            .index = index,
        }) catch unreachable;
        compiler.function.upvalueCount += 1;
        return @intCast(compiler.upvalues.items.len - 1);
    }

    fn resolveUpvalue(self: *Parser, compiler: *Compiler, name: str) ?Code {
        if (compiler.enclosing) |enclosing| {
            if (self.resolveLocal(enclosing, name)) |local| {
                enclosing.locals.items[local].isCaptured = true;
                return self.addUpvalue(compiler, local, true);
            }
            if (self.resolveUpvalue(enclosing, name)) |upvalue| {
                return self.addUpvalue(compiler, upvalue, false);
            }
        }

        return null;
    }

    fn namedVariable(self: *Parser, name: Token, can_assign: bool) LoxError!void {
        var getOp: OPCode = undefined;
        var setOp: OPCode = undefined;
        var arg: Code = undefined;

        if (self.resolveLocal(self.compiler, name.name)) |a| {
            getOp = .GetLocal;
            setOp = .SetLocal;
            arg = a;
        } else if (self.resolveUpvalue(self.compiler, name.name)) |a| {
            getOp = .GetUpvalue;
            setOp = .SetUpvalue;
            arg = a;
        } else {
            getOp = .GetGlobal;
            setOp = .SetGlobal;
            arg = try self.identifierConstant(&name);
        }

        if (can_assign and self.match(.EQUAL)) {
            try self.expression();
            try self.emitBytes(@intFromEnum(setOp), arg);
        } else {
            try self.emitBytes(@intFromEnum(getOp), arg);
        }
    }

    fn variable(self: *Parser, can_assign: bool) LoxError!void {
        try self.namedVariable(self.previous, can_assign);
    }

    fn addLocal(self: *Parser, name: Token) LoxError!void {
        if (self.compiler.locals.items.len == MAX_LOCALS)
            return self.error_("Too many local variables in function.", .{});
        self.compiler.locals.append(Local{
            .name = name,
            .depth = -1,
            .isCaptured = false,
        }) catch return LoxError.OutOfMemoryError;
    }

    fn markInitialized(self: *Parser) void {
        if (self.compiler.scopeDepth == 0) return;
        self.compiler.locals.items[self.compiler.locals.items.len - 1].depth =
            @intCast(self.compiler.scopeDepth);
    }

    fn identifiersEqual(a: *Token, b: *const Token) bool {
        return std.mem.eql(u8, a.name, b.name);
    }

    fn declareVariable(self: *Parser) LoxError!void {
        if (self.compiler.scopeDepth == 0) return;

        const name = &self.previous;
        var i = @as(i32, @intCast(self.compiler.locals.items.len)) - 1;
        while (i >= 0) : (i -= 1) {
            const local = &self.compiler.locals.items[@intCast(i)];
            if (local.depth != -1 and local.depth < self.compiler.scopeDepth)
                break;
            if (identifiersEqual(name, &local.name))
                self.error_("Already a variable with this name in this scope.", .{});
        }

        try self.addLocal(name.*);
    }

    fn parseVariable(self: *Parser, errorMessage: str) LoxError!Code {
        self.consume(.IDENTIFIER, errorMessage);

        try self.declareVariable();
        if (self.compiler.scopeDepth > 0) return 0;

        return try self.identifierConstant(&self.previous);
    }

    fn expression(self: *Parser) LoxError!void {
        try self.parsePrecedence(.ASSIGNMENT);
    }

    fn grouping(self: *Parser, _: bool) LoxError!void {
        try self.expression();
        self.consume(.RIGHT_PAREN, "Expect ')' after expression.");
    }

    fn unary(self: *Parser, _: bool) LoxError!void {
        const operatorType: TokenType = self.previous.Type;
        try self.parsePrecedence(.UNARY);
        try switch (operatorType) {
            .MINUS => self.emitByte(@intFromEnum(OPCode.Negate)),
            .BANG => self.emitByte(@intFromEnum(OPCode.Not)),
            else => unreachable,
        };
    }

    fn binary(self: *Parser, _: bool) LoxError!void {
        const operatorType: TokenType = self.previous.Type;
        const rule: ParseRule = getRule(operatorType);
        try self.parsePrecedence(@enumFromInt(@intFromEnum(rule.precedence) + 1));

        try switch (operatorType) {
            .PLUS => self.emitByte(@intFromEnum(OPCode.Add)),
            .MINUS => self.emitByte(@intFromEnum(OPCode.Sub)),
            .STAR => self.emitByte(@intFromEnum(OPCode.Mul)),
            .SLASH => self.emitByte(@intFromEnum(OPCode.Div)),
            .BANG_EQUAL => self.emitBytes(@intFromEnum(OPCode.Equal), @intFromEnum(OPCode.Not)),
            .EQUAL_EQUAL => self.emitByte(@intFromEnum(OPCode.Equal)),
            .GREATER => self.emitByte(@intFromEnum(OPCode.Greater)),
            .GREATER_EQUAL => self.emitBytes(@intFromEnum(OPCode.Less), @intFromEnum(OPCode.Not)),
            .LESS => self.emitByte(@intFromEnum(OPCode.Less)),
            .LESS_EQUAL => self.emitBytes(@intFromEnum(OPCode.Greater), @intFromEnum(OPCode.Not)),
            else => unreachable,
        };
    }

    fn argumentList(self: *Parser) LoxError!Code {
        var argCount: Code = 0;
        if (!self.check(.RIGHT_PAREN)) {
            var do = true;
            while (do or self.match(.COMMA)) {
                do = false;
                try self.expression();
                argCount += 1;
                if (argCount == 255)
                    self.error_("Can't have more than 255 arguments.", .{});
            }
        }
        self.consume(.RIGHT_PAREN, "Expect ')' after arguments.");
        return argCount;
    }

    fn call(self: *Parser, _: bool) LoxError!void {
        const argCount = try self.argumentList();
        try self.emitBytes(@intFromEnum(OPCode.Call), argCount);
    }

    fn dot(self: *Parser, canAssign: bool) LoxError!void {
        self.consume(.IDENTIFIER, "Expect property name after '.'.");
        const name = try self.identifierConstant(&self.previous);

        if (canAssign and self.match(.EQUAL)) {
            try self.expression();
            try self.emitBytes(@intFromEnum(OPCode.SetProperty), name);
        } else if (self.match(.LEFT_PAREN)) {
            const argCount = try self.argumentList();
            try self.emitBytes(@intFromEnum(OPCode.Invoke), name);
            try self.emitByte(argCount);
        } else {
            try self.emitBytes(@intFromEnum(OPCode.GetProperty), name);
        }
    }

    fn literal(self: *Parser, _: bool) LoxError!void {
        try switch (self.previous.Type) {
            .NIL => self.emitByte(@intFromEnum(OPCode.Nil)),
            .TRUE => self.emitByte(@intFromEnum(OPCode.True)),
            .FALSE => self.emitByte(@intFromEnum(OPCode.False)),
            else => unreachable,
        };
    }

    fn endCompiler(self: *Parser) LoxError!?*Function {
        const func = self.compiler.function;
        try self.emitReturn();
        if (comptime DEBUG_PRINT_CODE)
            if (!self.hadError)
                self.currentChunk().disassemble(if (func.name) |name| name.bytes else "<script>");
        if (self.compiler.enclosing) |compiler| {
            self.compiler = compiler;
        }
        return func;
    }

    fn check(self: *Parser, t: TokenType) bool {
        return self.current.Type == t;
    }

    fn match(self: *Parser, t: TokenType) bool {
        if (!self.check(t)) return false;
        self.advance();
        return true;
    }

    fn expressionStatement(self: *Parser) LoxError!void {
        try self.expression();
        self.consume(.SEMICOLON, "Expect ';' after expression.");
        try self.emitByte(@intFromEnum(OPCode.Pop));
    }

    fn block(self: *Parser) LoxError!void {
        while (!self.check(.RIGHT_BRACE) and !self.check(.EOF)) {
            try self.declaration();
        }
        self.consume(.RIGHT_BRACE, "Expect '}' after block.");
    }

    fn beginScope(self: *Parser) void {
        self.compiler.scopeDepth += 1;
    }

    fn endScope(self: *Parser) LoxError!void {
        self.compiler.scopeDepth -= 1;

        var locals = &self.compiler.locals;
        while (self.compiler.locals.items.len > 0 and
            locals.items[locals.items.len - 1].depth >
            self.compiler.scopeDepth)
        {
            if (locals.items[locals.items.len - 1].isCaptured) {
                try self.emitByte(@intFromEnum(OPCode.CloseUpvalue));
            } else {
                try self.emitByte(@intFromEnum(OPCode.Pop));
            }
            _ = self.compiler.locals.pop();
        }
    }

    fn patchJump(self: *Parser, offset: u32) void {
        // -2 to adjust for the bytecode for the jump offset itself.
        var jump = self.currentChunk().code.items.len - offset - 2;

        if (jump > std.math.maxInt(u16)) {
            self.error_("Too much code to jump over.", .{});
        }

        self.currentChunk().code.items[offset] = @intCast((jump >> 8) & 0xff);
        self.currentChunk().code.items[offset + 1] = @intCast(jump & 0xff);
    }

    fn emitJump(self: *Parser, instruction: OPCode) LoxError!u32 {
        try self.emitByte(@intFromEnum(instruction));
        try self.emitByte(0xff);
        try self.emitByte(0xff);
        return @as(u32, @intCast(self.currentChunk().code.items.len)) - 2;
    }

    fn ifStatement(self: *Parser) LoxError!void {
        self.consume(.LEFT_PAREN, "Expect '(' after 'if'.");
        try self.expression();
        self.consume(.RIGHT_PAREN, "Expect ')' after condition.");

        const thenJump = try self.emitJump(.JumpIfFalse);
        try self.emitByte(@intFromEnum(OPCode.Pop));
        try self.statement();

        const elseJump = try self.emitJump(.Jump);
        self.patchJump(thenJump);
        try self.emitByte(@intFromEnum(OPCode.Pop));

        try if (self.match(.ELSE)) self.statement();
        self.patchJump(elseJump);
    }

    fn emitLoop(self: *Parser, loopStart: u32) LoxError!void {
        try self.emitByte(@intFromEnum(OPCode.Loop));

        const offset = self.currentChunk().code.items.len - loopStart + 2;
        if (offset > std.math.maxInt(u16)) self.error_("Loop body too large.", .{});

        try self.emitByte(@intCast((offset >> 8) & 0xff));
        try self.emitByte(@intCast(offset & 0xff));
    }

    fn whileStatement(self: *Parser) LoxError!void {
        const loopStart: u32 = @intCast(self.currentChunk().code.items.len);
        self.consume(.LEFT_PAREN, "Expect '(' after 'while'.");
        try self.expression();
        self.consume(.RIGHT_PAREN, "Expect ')' after condition.");

        const exitJump = try self.emitJump(.JumpIfFalse);
        try self.emitByte(@intFromEnum(OPCode.Pop));
        try self.statement();
        try self.emitLoop(loopStart);

        self.patchJump(exitJump);
        try self.emitByte(@intFromEnum(OPCode.Pop));
    }

    fn forStatement(self: *Parser) LoxError!void {
        self.beginScope();
        self.consume(.LEFT_PAREN, "Expect '(' after 'for'.");
        if (self.match(.SEMICOLON)) {
            // No initializer.
        } else if (self.match(.VAR)) {
            try self.varDeclaration();
        } else {
            try self.expressionStatement();
        }

        var loopStart: u32 = @intCast(self.currentChunk().code.items.len);
        var exitJump: i32 = -1;
        if (!self.match(.SEMICOLON)) {
            try self.expression();
            self.consume(.SEMICOLON, "Expect ';' after loop condition.");
            // Jump out of the loop if the condition is false.
            exitJump = @intCast(try self.emitJump(.JumpIfFalse));
            try self.emitByte(@intFromEnum(OPCode.Pop)); // Condition.
        }

        if (!self.match(.RIGHT_PAREN)) {
            const bodyJump = try self.emitJump(.Jump);
            const incrementStart: u32 = @intCast(self.currentChunk().code.items.len);
            try self.expression();
            try self.emitByte(@intFromEnum(OPCode.Pop));
            self.consume(.RIGHT_PAREN, "Expect ')' after for clauses.");

            try self.emitLoop(loopStart);
            loopStart = incrementStart;
            self.patchJump(bodyJump);
        }

        try self.statement();
        try self.emitLoop(loopStart);

        if (exitJump != -1) {
            self.patchJump(@intCast(exitJump));
            try self.emitByte(@intFromEnum(OPCode.Pop)); // Condition.
        }

        try self.endScope();
    }

    fn returnStatement(self: *Parser) LoxError!void {
        if (self.match(.SEMICOLON)) {
            try self.emitReturn();
        } else {
            try self.expression();
            self.consume(.SEMICOLON, "Expect ';' after return value.");
            try self.emitByte(@intFromEnum(OPCode.Ret));
        }
    }

    fn arrayIndex(self: *Parser, canAssign: bool) LoxError!void {
        try self.expression();
        self.consume(.RIGHT_BRACKET, "Expect ']' after an index.");
        if (canAssign and self.match(.EQUAL)) {
            try self.expression();
            try self.emitByte(@intFromEnum(OPCode.SetIndex));
        } else if (self.match(.LEFT_PAREN)) {
            const argCount = try self.argumentList();
            try self.emitByte(@intFromEnum(OPCode.InvokeFromArray));
            try self.emitByte(argCount);
        } else {
            try self.emitByte(@intFromEnum(OPCode.GetIndex));
        }
    }

    fn array(self: *Parser, _: bool) LoxError!void {
        var elemCount: Code = 0;
        if (!self.check(.RIGHT_BRACKET) and !self.check(.EOF)) {
            while (!self.check(.RIGHT_BRACKET) and !self.check(.EOF)) {
                try self.expression();
                elemCount += 1;
                if (elemCount == 255)
                    self.error_("Can't have more than 255 array elements.", .{});
                if (!self.check(.RIGHT_BRACKET) and !self.check(.EOF))
                    self.consume(.COMMA, "Expect ',' after value expression.");
            }
        }

        self.consume(.RIGHT_BRACKET, "Expect ']' after expressions.");
        const arr = (try Array.new(self.vm, elemCount)).obj.asValue();
        try self.emitBytes(@intFromEnum(OPCode.Array), try self.makeConstant(arr));
        try self.emitByte(elemCount);
    }

    fn statement(self: *Parser) LoxError!void {
        if (self.match(.IF)) {
            try self.ifStatement();
        } else if (self.match(.RETURN)) {
            try self.returnStatement();
        } else if (self.match(.WHILE)) {
            try self.whileStatement();
        } else if (self.match(.FOR)) {
            try self.forStatement();
        } else if (self.match(.LEFT_BRACE)) {
            self.beginScope();
            try self.block();
            try self.endScope();
        } else {
            try self.expressionStatement();
        }
    }

    fn synchronize(self: *Parser) void {
        self.panicMode = false;
        while (self.current.Type != .EOF) {
            if (self.previous.Type == .SEMICOLON) return;
            switch (self.current.Type) {
                .FUN, .VAR, .FOR, .IF, .WHILE, .RETURN => return,
                else => {},
            }

            self.advance();
        }
    }

    fn defineVariable(self: *Parser, global: Code) LoxError!void {
        if (self.compiler.scopeDepth > 0) {
            self.markInitialized();
            return;
        }
        try self.emitBytes(@intFromEnum(OPCode.DefineGlobal), global);
    }

    fn and_(self: *Parser, _: bool) LoxError!void {
        const end_jump = try self.emitJump(.JumpIfFalse);
        try self.emitByte(@intFromEnum(OPCode.Pop));
        try self.parsePrecedence(.AND);
        self.patchJump(end_jump);
    }

    fn or_(self: *Parser, _: bool) LoxError!void {
        const elseJump = try self.emitJump(.JumpIfFalse);
        const endJump = try self.emitJump(.Jump);

        self.patchJump(elseJump);
        try self.emitByte(@intFromEnum(OPCode.Pop));

        try self.parsePrecedence(.OR);
        self.patchJump(endJump);
    }

    fn varDeclaration(self: *Parser) LoxError!void {
        const global = try self.parseVariable("Expect variable name.");

        if (self.match(.EQUAL))
            try self.expression()
        else
            try self.emitByte(@intFromEnum(OPCode.Nil));

        self.consume(.SEMICOLON, "Expect ';' after variable declaration.");

        try self.defineVariable(global);
    }

    fn parseFunction(self: *Parser, isAnonymous: bool) LoxError!void {
        self.consume(
            .LEFT_PAREN,
            if (isAnonymous) "Expect '(' after 'fun'." else "Expect '(' after function name.",
        );
        if (!self.check(.RIGHT_PAREN)) {
            var do = true; // do while when?
            while (do or self.match(.COMMA)) {
                do = false;
                self.compiler.function.arity += 1;
                if (self.compiler.function.arity > 255) {
                    self.errorAtCurrent("Can't have more than 255 parameters.");
                }
                const constant = try self.parseVariable("Expect parameter name.");
                try self.defineVariable(constant);
            }
        }

        self.consume(.RIGHT_PAREN, "Expect ')' after parameters.");
        self.consume(.LEFT_BRACE, "Expect '{' before function body.");

        try self.block();
    }

    fn parseLambda(self: *Parser) LoxError!void {
        if (!self.check(.ARROW)) {
            var do = true;
            while (do or self.match(.COMMA)) {
                do = false;
                self.compiler.function.arity += 1;
                if (self.compiler.function.arity > 255) {
                    self.errorAtCurrent("Can't have more than 255 parameters.");
                }
                const constant = try self.parseVariable("Expect parameter name.");
                try self.defineVariable(constant);
            }
        }

        self.consume(.ARROW, "Expect '->' after parameter list fo short lambda.");

        try self.expression();
        try self.emitByte(@intFromEnum(OPCode.Ret));
    }

    fn function(self: *Parser, Type: FunctionType) LoxError!void {
        var compiler = try Compiler.init(self.vm, Type, self.compiler);
        defer compiler.deinit();
        self.compiler = &compiler;
        self.compiler.function.name = try String.copy(
            self.vm,
            switch (Type) {
                .Function => self.previous.name,
                else => "anonymous",
            },
        );
        self.beginScope();

        try switch (Type) {
            .Function => self.parseFunction(false),
            .Anonymous => self.parseFunction(true),
            .Lambda => self.parseLambda(),
            else => self.parseFunction(false),
        };

        if (try self.endCompiler()) |func| {
            const constant = try self.makeConstant(func.obj.asValue());
            try self.emitBytes(@intFromEnum(OPCode.Closure), constant);
            var i: u32 = 0;
            while (i < func.upvalueCount) : (i += 1) {
                try self.emitByte(if (compiler.upvalues.items[i].isLocal) 1 else 0);
                try self.emitByte(compiler.upvalues.items[i].index);
            }
        }
    }

    fn lambda(self: *Parser, _: bool) LoxError!void {
        try self.function(.Lambda);
    }

    fn anonymousFunction(self: *Parser, _: bool) LoxError!void {
        try self.function(.Anonymous);
    }

    fn funDeclaration(self: *Parser) LoxError!void {
        if (self.check(.IDENTIFIER)) {
            const global = try self.parseVariable("Expect function name.");
            self.markInitialized();
            try self.function(.Function);
            try self.defineVariable(global);
        } else {
            try self.parsePrecedence(.ASSIGNMENT);
        }
    }

    fn hashTable(self: *Parser) LoxError!void {
        self.consume(.LEFT_BRACE, "");

        var kvCount: u32 = 0;
        while (!self.check(.RIGHT_BRACE) and !self.check(.EOF)) : (kvCount += 2) {
            if (self.match(.DOT)) {
                self.consume(.IDENTIFIER, "Expect property name after '.'.");
                const s = try String.copy(self.vm, self.previous.name);
                try self.emitConstant(s.obj.asValue());
            } else {
                try self.expression();
            }
            self.consume(.FAT_ARROW, "Expect '=>' after key expression.");
            try self.expression();
            if (!self.check(.RIGHT_BRACE) and !self.check(.EOF))
                self.consume(.COMMA, "Expect ',' after value expression.");
        }

        self.consume(.RIGHT_BRACE, "Expect '}' after table body.");
        if (kvCount % 2 != 0) self.error_("Missing value expression", .{});
        if (kvCount > 255) self.error_("too many key-value expressions", .{});
        const table = (try HashTable.new(self.vm, .Table)).obj.asValue();
        try self.emitBytes(@intFromEnum(OPCode.Table), try self.makeConstant(table));
        try self.emitByte(@intCast(kvCount));
    }

    fn hashSet(self: *Parser) LoxError!void {
        self.consume(.LEFT_BRACKET, "");

        var valueCount: u32 = 0;
        while (!self.check(.RIGHT_BRACKET) and !self.check(.EOF)) : (valueCount += 1) {
            try self.expression();
            if (!self.check(.RIGHT_BRACKET) and !self.check(.EOF))
                self.consume(.COMMA, "Expect ',' after value expression.");
        }

        self.consume(.RIGHT_BRACKET, "Expect ']' after set elements.");
        if (valueCount > 255) self.error_("too many value expressions", .{});
        const table = (try HashTable.new(self.vm, .HashSet)).obj.asValue();
        try self.emitBytes(@intFromEnum(OPCode.HashSet), try self.makeConstant(table));
        try self.emitByte(@intCast(valueCount));
    }

    fn dispatch(self: *Parser, _: bool) LoxError!void {
        if (self.check(.LEFT_BRACE)) {
            try self.hashTable();
        } else if (self.check(.LEFT_BRACKET)) {
            try self.hashSet();
        } else {
            return self.error_("expected '{{' or '[', got '{s}'", .{self.current.name});
        }
    }

    fn declaration(self: *Parser) LoxError!void {
        if (self.match(.FUN)) {
            try self.funDeclaration();
        } else if (self.match(.VAR)) {
            try self.varDeclaration();
        } else {
            try self.statement();
        }
        if (self.panicMode) self.synchronize();
    }
};

pub fn compile(source: *const str, vm: *VM) LoxError!?*Function {
    const scanner = Scanner.init(source);
    var compiler = try Compiler.init(vm, .Script, null);
    defer compiler.deinit();
    var parser = Parser.init(scanner, vm, &compiler);
    defer parser.deinit();
    vm.parser = &parser;
    defer vm.parser = null;

    _ = parser.advance();

    while (!parser.match(.EOF)) {
        try parser.declaration();
    }

    const function = parser.endCompiler();

    return if (parser.hadError) null else function;
}
