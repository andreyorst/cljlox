const std = @import("std");
const Allocator = std.mem.Allocator;

const object = @import("object.zig");
const Obj = object.Obj;
const ObjType = object.ObjType;
const String = object.String;
const Function = object.Function;
const Closure = object.Closure;
const NativeFunction = object.NativeFunction;

const hashUint64 = std.hash.Murmur3_32.hashUint64;

const VM = @import("vm.zig").VM;

const common = @import("common.zig");
const typeNameUnqualified = common.typeNameUnqualified;
const LoxError = common.LoxError;
const str = common.str;

const NAN_BOXING = true;

fn hashNumber(x: f64) u32 {
    return hashUint64(@bitCast(x));
}

pub fn tostring_(value: Value, allocator: Allocator) !str {
    var array_list = std.ArrayList(u8).init(allocator);
    defer array_list.deinit();

    try value.format("{}", .{}, array_list.writer());

    var escaped: bool = false;
    var offset: usize = 0;
    for (0..array_list.items.len) |i| {
        const char = array_list.items[i + offset];
        if (!escaped and (char == '"')) {
            escaped = true;
            try array_list.insert(i + offset, '\\');
            offset += 1;
        } else {
            escaped = false;
        }
    }

    return try array_list.toOwnedSlice();
}

pub const NanBoxedValue = packed struct {
    data: u64,

    const SIGN_BIT: u64 = 0x8000000000000000;
    const QNAN: u64 = 0x7ffc000000000000;

    pub const Nil = NanBoxedValue{ .data = QNAN | 1 };
    pub const True = NanBoxedValue{ .data = QNAN | 3 };
    pub const False = NanBoxedValue{ .data = QNAN | 2 };

    pub fn isFalsey(self: NanBoxedValue) bool {
        if (self.is(.Bool)) return !self.as(bool);
        if (self.is(.Nil)) return true;
        return false;
    }

    pub fn equal(self: NanBoxedValue, other: NanBoxedValue) bool {
        if (self.is(.Number) and other.is(.Number)) {
            return self.as(f64) == other.as(f64);
        } else if (self.is(.Bool) and other.is(.Bool)) {
            return self.as(bool) == other.as(bool);
        } else if (self.is(.Nil) and other.is(.Nil)) {
            return true;
        } else if (self.is(.Obj) and other.is(.Obj)) {
            return self.as(*Obj).equal(other.as(*Obj));
        }
        return false;
    }

    pub inline fn is(self: NanBoxedValue, comptime target: @TypeOf(.enum_literal)) bool {
        if (comptime @hasField(ObjType, @tagName(target))) {
            if (self.is(.Obj)) {
                return @as(
                    *Obj,
                    @ptrFromInt(@as(usize, @intCast(self.data & ~(SIGN_BIT | QNAN)))),
                ).objType == target;
            } else return false;
        } else {
            return switch (target) {
                .Bool => (self.data & False.data) == False.data,
                .Nil => self.data == Nil.data,
                .Number => (self.data & QNAN) != QNAN,
                .Obj => (self.data & (QNAN | SIGN_BIT)) == (QNAN | SIGN_BIT),
                else => unreachable,
            };
        }
    }

    pub inline fn as(self: NanBoxedValue, comptime T: type) T {
        return if (comptime @hasField(ObjType, typeNameUnqualified(T)))
            @as(*Obj, @ptrFromInt(@as(usize, @intCast(self.data & ~(SIGN_BIT | QNAN))))).as(
                switch (@typeInfo(T)) {
                    .Pointer => |p| p.child,
                    else => T,
                },
            )
        else switch (T) {
            f64 => @as(f64, @bitCast(self.data)),
            bool => self.data == True.data,
            void => Nil.data,
            *Obj => @as(*Obj, @ptrFromInt(@as(usize, @intCast(self.data & ~(SIGN_BIT | QNAN))))),
            else => unreachable,
        };
    }

    pub inline fn from(x: anytype) NanBoxedValue {
        return switch (@TypeOf(x)) {
            usize, i32, comptime_int => NanBoxedValue{ .data = @as(u64, @bitCast(@as(f64, @floatFromInt(x)))) },
            f64, comptime_float => NanBoxedValue{ .data = @as(u64, @bitCast(@as(f64, x))) },
            bool => if (x) True else False,
            void => Nil,
            *Obj => NanBoxedValue{ .data = SIGN_BIT | QNAN | @intFromPtr(x) },
            else => unreachable,
        };
    }

    pub fn mark(self: NanBoxedValue, vm: *VM) !void {
        if (self.is(.Obj))
            try self.as(*Obj).mark(vm);
    }

    pub fn format(
        self: NanBoxedValue,
        comptime fmt: []const u8,
        options: std.fmt.FormatOptions,
        out_stream: anytype,
    ) !void {
        _ = fmt;
        _ = options;
        if (self.is(.Number)) {
            try out_stream.print("{d}", .{self.as(f64)});
        } else if (self.is(.Bool)) {
            try out_stream.print("{}", .{self.as(bool)});
        } else if (self.is(.Nil)) {
            try out_stream.print("nil", .{});
        } else {
            try self.as(*Obj).print(out_stream);
        }
    }

    pub fn tostring(self: NanBoxedValue, allocator: Allocator) !str {
        return tostring_(self, allocator);
    }

    pub fn getHash(self: NanBoxedValue) u32 {
        if (self.is(.Obj))
            return self.as(*Obj).getHash();
        if (self.is(.Number))
            return hashNumber(self.as(f64));
        unreachable;
    }

    pub fn get(self: NanBoxedValue, index: Value, vm: *VM) LoxError!Value {
        if (self.is(.Obj))
            return try self.as(*Obj).get(index, vm);
        return vm.runtimeError(
            "attempt to index a {s} value",
            .{if (self.is(.Number)) "number" else if (self.is(.Bool)) "boolean" else "nil"},
        );
    }

    pub fn set(self: NanBoxedValue, index: Value, value: Value, vm: *VM) LoxError!void {
        if (self.is(.Obj)) {
            try self.as(*Obj).set(index, value, vm);
        } else return vm.runtimeError(
            "attempt to index a {s} value",
            .{if (self.is(.Number)) "number" else if (self.is(.Bool)) "boolean" else "nil"},
        );
    }

    pub fn kind(self: NanBoxedValue) str {
        return if (self.is(.Obj))
            self.as(*Obj).kind()
        else if (self.is(.Number))
            "number"
        else if (self.is(.Bool))
            "bool"
        else
            "nil";
    }
};

pub const UnionValue = union(enum) {
    Number: f64,
    Bool: bool,
    Nil,
    Obj: *Obj,

    pub const Nil = UnionValue{ .Nil = undefined };
    pub const True = UnionValue{ .Bool = true };
    pub const False = UnionValue{ .Bool = false };

    pub inline fn isFalsey(self: UnionValue) bool {
        return switch (self) {
            .Bool => |value| !value,
            .Nil => true,
            else => false,
        };
    }

    pub inline fn equal(self: UnionValue, other: UnionValue) bool {
        return switch (self) {
            .Bool => |a| switch (other) {
                .Bool => |b| a == b,
                else => false,
            },
            .Number => |a| switch (other) {
                .Number => |b| a == b,
                else => false,
            },
            .Nil => switch (other) {
                .Nil => true,
                else => false,
            },
            .Obj => |a| switch (other) {
                .Obj => |b| a.equal(b),
                else => false,
            },
        };
    }

    pub inline fn is(self: UnionValue, comptime target: @TypeOf(.enum_literal)) bool {
        if (comptime @hasField(ObjType, @tagName(target))) {
            return self == .Obj and self.Obj.objType == target;
        } else {
            return self == target;
        }
    }

    pub inline fn as(self: UnionValue, comptime T: type) T {
        return if (comptime @hasField(ObjType, typeNameUnqualified(T)))
            self.Obj.as(switch (@typeInfo(T)) {
                .Pointer => |p| p.child,
                else => T,
            })
        else switch (T) {
            f64 => self.Number,
            bool => self.Bool,
            void => self.Nil,
            *Obj => self.Obj,
            else => unreachable,
        };
    }

    pub inline fn from(x: anytype) UnionValue {
        return switch (@TypeOf(x)) {
            usize, i32, comptime_int => UnionValue{ .Number = @as(f64, @floatFromInt(x)) },
            f64, comptime_float => UnionValue{ .Number = x },
            bool => UnionValue{ .Bool = x },
            void => Nil,
            *Obj => UnionValue{ .Obj = x },
            else => unreachable,
        };
    }

    pub fn mark(self: UnionValue, vm: *VM) !void {
        switch (self) {
            .Obj => |obj| try obj.mark(vm),
            else => {},
        }
    }

    pub fn getHash(self: UnionValue) u32 {
        return switch (self) {
            .Obj => self.as(*Obj).getHash(),
            .Number => |x| hashNumber(x),
            else => unreachable,
        };
    }

    pub fn format(
        self: UnionValue,
        comptime fmt: []const u8,
        options: std.fmt.FormatOptions,
        out_stream: anytype,
    ) !void {
        _ = fmt;
        _ = options;
        switch (self) {
            .Number => |value| try out_stream.print("{d}", .{value}),
            .Bool => |value| try out_stream.print("{}", .{value}),
            .Nil => try out_stream.print("nil", .{}),
            .Obj => |obj| try obj.print(out_stream),
        }
    }

    pub fn tostring(self: UnionValue, allocator: Allocator) !str {
        return tostring_(self, allocator);
    }

    pub fn get(self: UnionValue, index: UnionValue, vm: *VM) LoxError!UnionValue {
        if (self.is(.Obj))
            return try self.as(*Obj).get(index, vm);
        return vm.runtimeError(
            "attempt to index a {s} value",
            .{
                switch (self) {
                    .Number => "number",
                    .Bool => "boolean",
                    .Nil => "nil",
                    else => unreachable,
                },
            },
        );
    }

    pub fn set(self: UnionValue, index: UnionValue, value: UnionValue, vm: *VM) LoxError!void {
        if (self.is(.Obj)) {
            try self.as(*Obj).set(index, value, vm);
            return;
        }
        return vm.runtimeError(
            "attempt to index a {s} value",
            .{
                switch (self) {
                    .Number => "number",
                    .Bool => "boolean",
                    .Nil => "nil",
                    else => unreachable,
                },
            },
        );
    }

    pub fn kind(self: UnionValue) str {
        if (self.is(.Obj))
            return self.as(*Obj).kind();
        return switch (self) {
            .Number => "number",
            .Bool => "boolean",
            .Nil => "nil",
            else => unreachable,
        };
    }
};

pub const Value = if (NAN_BOXING) NanBoxedValue else UnionValue;
