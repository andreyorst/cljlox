const std = @import("std");
const hashFn = std.hash.Fnv1a_32.hash;
const ArrayList = std.ArrayList;
const hashUint64 = std.hash.Murmur3_32.hashUint64;

const VM = @import("vm.zig").VM;
const Chunk = @import("chunk.zig").Chunk;
const Table = @import("table.zig").Table;

const Value = @import("value.zig").Value;
const Nil = Value.Nil;

const common = @import("common.zig");
const str = common.str;
const eprint = common.eprint;
const LoxError = common.LoxError;
const DEBUG_LOG_GC = common.DEBUG_LOG_GC;
const hashNumber = common.hashNumber;

pub const ObjType = enum {
    String,
    Function,
    NativeFunction,
    Closure,
    Upvalue,
    HashTable,
    Array,
    pub fn from(comptime T: type) ObjType {
        const name = comptime common.typeNameUnqualified(T);
        if (@hasField(ObjType, name)) {
            return switch (T) {
                String => .String,
                Function => .Function,
                NativeFunction => .NativeFunction,
                Closure => .Closure,
                Upvalue => .Upvalue,
                HashTable => .HashTable,
                Array => .Array,
                else => @compileError("unsupported type" ++ @typeName(T)),
            };
        }
        @compileError(@typeName(ObjType) ++ " has no such field:" ++ name);
    }
};

pub const Obj = struct {
    objType: ObjType,
    isMarked: bool,
    next: ?*Obj,
    nextGray: ?*Obj,
    vm: *VM,

    pub fn allocate(vm: *VM, comptime T: type) LoxError!*Obj {
        const ptr = vm.allocator.create(T) catch {
            eprint("OOME: can't allocate object", .{});
            return LoxError.OutOfMemoryError;
        };

        ptr.obj = Obj{
            .objType = ObjType.from(T),
            .next = vm.objects,
            .isMarked = false,
            .nextGray = null,
            .vm = vm,
        };

        vm.objects = &ptr.obj;

        if (comptime DEBUG_LOG_GC) eprint("{*} allocate {} for {s}\n", .{ &ptr.obj, @sizeOf(T), @typeName(T) });

        return &ptr.obj;
    }

    pub fn mark(self: *Obj, vm: *VM) !void {
        if (self.isMarked) return;
        if (comptime DEBUG_LOG_GC) eprint("{*} mark {}\n", .{ self, self.asValue() });
        self.isMarked = true;
        try vm.grayStack.append(self);
    }

    pub fn blacken(self: *Obj, vm: *VM) !void {
        switch (self.objType) {
            .NativeFunction, .String => return,
            .Upvalue => try self.as(Upvalue).closed.mark(vm),
            .Function => {
                const function = self.as(Function);
                if (function.name) |name| {
                    try name.obj.mark(vm);
                }
                try function.markConstants(vm);
            },
            .Closure => {
                const closure = self.as(Closure);
                try closure.function.obj.mark(vm);
                for (0..closure.function.upvalueCount) |i| {
                    if (closure.upvalues[i]) |upvalue|
                        try upvalue.obj.mark(vm);
                }
            },
            .HashTable => {
                const htable = self.as(HashTable);
                try htable.fields.mark(vm);
            },
            .Array => {
                const arr = self.as(Array);
                try arr.markValues(vm);
            },
        }
        if (comptime DEBUG_LOG_GC) eprint("{*} blacken {}\n", .{ self, self.asValue() });
    }

    pub fn is(self: *Obj, objType: ObjType) bool {
        return self.objType == objType;
    }

    pub fn as(self: *Obj, comptime T: type) *T {
        return @fieldParentPtr(T, "obj", self);
    }

    pub fn destroy(self: *Obj, vm: *VM) void {
        if (comptime DEBUG_LOG_GC) eprint("{*} free type {s}\n", .{ self, @tagName(self.objType) });
        switch (self.objType) {
            .String => self.as(String).destroy(vm),
            .Function => self.as(Function).destroy(vm),
            .NativeFunction => self.as(NativeFunction).destroy(vm),
            .Closure => self.as(Closure).destroy(vm),
            .Upvalue => self.as(Upvalue).destroy(vm),
            .HashTable => self.as(HashTable).destroy(vm),
            .Array => self.as(Array).destroy(vm),
        }
    }

    pub fn equal(self: *Obj, other: *Obj) bool {
        if (self.objType != other.objType) return false;

        if (self == other) return true;

        return switch (self.objType) {
            .String => self.as(String).equal(other.as(String)),
            .Array => self.as(Array).equal(other.as(Array)),
            .HashTable => self.as(HashTable).equal(other.as(HashTable)),
            else => false,
        };
    }

    pub fn asValue(self: *Obj) Value {
        return Value.from(self);
    }

    pub fn getHash(self: *Obj) u32 {
        return switch (self.objType) {
            .String => self.as(String).hash,
            .HashTable, .Array, .Function, .NativeFunction => hashUint64(@intFromPtr(self)),
            else => unreachable,
        };
    }

    pub fn print(self: *Obj, out_stream: anytype) @TypeOf(out_stream).Error!void {
        try switch (self.objType) {
            .String => {
                const bytes = self.as(String).bytes;
                try out_stream.writeByte('"');
                var escaped = false;
                var offset: u32 = 0;
                for (0..bytes.len) |i| {
                    if (!escaped and bytes[i - offset] == '\\') {
                        escaped = true;
                    } else {
                        escaped = false;
                        try out_stream.writeByte(bytes[i - offset]);
                    }
                }
                try out_stream.writeByte('"');
            },
            .Function => if (self.as(Function).name) |name|
                out_stream.print("<fn {s}: 0x{x}>", .{ name.bytes, @intFromPtr(self) })
            else
                out_stream.print("<script>", .{}),
            .NativeFunction => out_stream.print(
                "<fn {s}: 0x{x}>",
                .{ self.as(NativeFunction).name, @intFromPtr(self) },
            ),
            .Upvalue => out_stream.print("upvalue", .{}),
            .Closure => self.as(Closure).function.obj.print(out_stream),
            .HashTable => blk: {
                const this = self.as(HashTable);
                var it = this.fields.hm.iterator();
                var first = true;
                switch (this.kind) {
                    .Table => {
                        try out_stream.print("#{{", .{});
                        while (it.next()) |kv| {
                            if (!first) try out_stream.print(", ", .{});
                            first = false;
                            if (kv.key_ptr.*.is(.String)) {
                                const k = kv.key_ptr.*.tostring(self.vm.allocator) catch unreachable;
                                defer self.vm.allocator.free(k);
                                try out_stream.print("{s} => ", .{k});
                            } else {
                                try out_stream.print("{} =>", .{kv.key_ptr.*});
                            }
                            if (kv.value_ptr.*.is(.String)) {
                                const v = kv.value_ptr.*.tostring(self.vm.allocator) catch unreachable;
                                defer self.vm.allocator.free(v);
                                try out_stream.print("{s}", .{v});
                            } else {
                                try out_stream.print("{}", .{kv.value_ptr.*});
                            }
                        }
                        break :blk out_stream.print("}}", .{});
                    },
                    .HashSet => {
                        try out_stream.print("#[", .{});
                        while (it.next()) |kv| {
                            if (!first) try out_stream.print(", ", .{});
                            first = false;
                            if (kv.key_ptr.*.is(.String)) {
                                const k = kv.key_ptr.*.tostring(self.vm.allocator) catch unreachable;
                                defer self.vm.allocator.free(k);
                                try out_stream.print("{s}", .{k});
                            } else {
                                try out_stream.print("{}", .{kv.key_ptr.*});
                            }
                        }
                        break :blk out_stream.print("]", .{});
                    },
                }
            },
            .Array => blk: {
                try out_stream.print("[", .{});
                for (self.as(Array).values.items, 0..) |value, i| {
                    if (i > 0) try out_stream.print(", ", .{});
                    if (value.is(.String)) {
                        const v = value.tostring(self.vm.allocator) catch unreachable;
                        defer self.vm.allocator.free(v);
                        try out_stream.print("{s}", .{v});
                    } else {
                        try out_stream.print("{}", .{value});
                    }
                }
                break :blk out_stream.print("]", .{});
            },
        };
    }

    pub fn get(self: *Obj, index: Value, vm: *VM) LoxError!Value {
        return switch (self.objType) {
            .Array => try self.as(Array).get(index, vm),
            .HashTable => try self.as(HashTable).get(index, vm),
            else => vm.runtimeError("attempt to index a {s} value", .{@tagName(self.objType)}),
        };
    }

    pub fn set(self: *Obj, index: Value, value: Value, vm: *VM) LoxError!void {
        switch (self.objType) {
            .Array => try self.as(Array).set(index, value, vm),
            .HashTable => try self.as(HashTable).set(index, value, vm),
            else => return vm.runtimeError("attempt to index a {s} value", .{@tagName(self.objType)}),
        }
    }

    pub fn kind(self: *Obj) str {
        return switch (self.objType) {
            .String => "string",
            .Closure, .Function, .NativeFunction => "function",
            .Upvalue => "upvalue",
            .HashTable => switch (self.as(HashTable).kind) {
                .Table => "table",
                .HashSet => "set",
            },
            .Array => "array",
        };
    }
};

pub const String = struct {
    const Self = @This();
    obj: Obj,
    hash: u32,
    bytes: []const u8,

    pub fn new(vm: *VM, bytes: str, hash: u32) LoxError!*Self {
        const obj = try Obj.allocate(vm, Self);
        const out = obj.as(Self);
        out.* = String{
            .obj = obj.*,
            .hash = hash,
            .bytes = bytes,
        };
        vm.push(obj.asValue());
        _ = try vm.strings.set(out, Nil);
        _ = vm.pop();
        return out;
    }

    pub fn take(vm: *VM, bytes: str) LoxError!*Self {
        const hash = hashFn(bytes);
        if (vm.strings.findString(bytes, hash)) |s| {
            vm.allocator.free(bytes);
            return s;
        }
        return try String.new(vm, bytes, hash);
    }

    pub fn copy(vm: *VM, source: str) LoxError!*Self {
        const hash = hashFn(source);
        if (vm.strings.findString(source, hash)) |s| {
            return s;
        }
        const buffer = vm.allocator.alloc(u8, source.len) catch {
            eprint("OOME: can't allocate string", .{});
            return LoxError.OutOfMemoryError;
        };
        std.mem.copy(u8, buffer, source);
        return String.new(vm, buffer, hash);
    }

    pub fn destroy(self: *Self, vm: *VM) void {
        vm.allocator.free(self.bytes);
        vm.allocator.destroy(self);
    }

    pub fn equal(self: *Self, other: *Self) bool {
        return (self.bytes.len == other.bytes.len) and
            std.mem.eql(u8, self.bytes, other.bytes);
    }

    pub fn mark(self: *Self, vm: *VM) !void {
        try self.obj.mark(vm);
    }

    pub fn isMarked(self: *Self) bool {
        return self.obj.isMarked;
    }

    pub fn getHash(self: *Self) u32 {
        return self.hash;
    }
};

pub const Function = struct {
    const Self = @This();
    obj: Obj,
    arity: u32,
    chunk: Chunk,
    upvalueCount: u32,
    name: ?*String,

    pub fn new(vm: *VM) LoxError!*Self {
        const obj = try Obj.allocate(vm, Self);
        const out = obj.as(Self);
        out.* = Self{
            .obj = obj.*,
            .arity = 0,
            .upvalueCount = 0,
            .chunk = Chunk.init(vm.allocator),
            .name = null,
        };
        return out;
    }

    pub fn destroy(self: *Self, vm: *VM) void {
        self.chunk.deinit();
        vm.allocator.destroy(self);
    }

    pub fn markConstants(self: *Self, vm: *VM) !void {
        for (self.chunk.constants.items) |constant| {
            try constant.mark(vm);
        }
    }
};

pub const NativeFn = *const fn (argCount: u8, args: [*]Value, vm: *VM) LoxError!Value;

pub const NativeFunction = struct {
    const Self = @This();
    obj: Obj,
    function: NativeFn,
    name: str,

    pub fn new(vm: *VM, function: NativeFn, name: str) LoxError!*Self {
        const obj = try Obj.allocate(vm, Self);
        const out = obj.as(Self);
        out.* = Self{
            .obj = obj.*,
            .function = function,
            .name = name,
        };
        return out;
    }

    pub fn destroy(self: *Self, vm: *VM) void {
        vm.allocator.destroy(self);
    }
};

pub const Closure = struct {
    const Self = @This();
    obj: Obj,
    function: *Function,
    upvalues: []?*Upvalue,

    pub fn new(vm: *VM, function: *Function) LoxError!*Self {
        const upvalues = vm.allocator.alloc(?*Upvalue, function.upvalueCount) catch {
            eprint("OOME: can't allocate upvalues", .{});
            return LoxError.OutOfMemoryError;
        };
        for (upvalues) |*upvalue| upvalue.* = null;

        const obj = try Obj.allocate(vm, Self);
        const out = obj.as(Self);
        out.* = Self{
            .obj = obj.*,
            .function = function,
            .upvalues = upvalues,
        };
        return out;
    }

    pub fn destroy(self: *Self, vm: *VM) void {
        vm.allocator.free(self.upvalues);
        vm.allocator.destroy(self);
    }
};

pub const Upvalue = struct {
    const Self = @This();
    obj: Obj,
    location: *Value,
    next: ?*Upvalue,
    closed: Value,

    pub fn new(vm: *VM, slot: *Value) LoxError!*Self {
        const obj = try Obj.allocate(vm, Self);
        const out = obj.as(Self);
        out.* = Self{
            .obj = obj.*,
            .location = slot,
            .next = null,
            .closed = Nil,
        };
        return out;
    }

    pub fn destroy(self: *Self, vm: *VM) void {
        vm.allocator.destroy(self);
    }
};

pub const HashTable = struct {
    const Self = @This();
    obj: Obj,
    fields: Table(Value, Value),
    kind: TableType,

    const TableType = enum { Table, HashSet };

    pub fn new(vm: *VM, kind: TableType) LoxError!*Self {
        const obj = try Obj.allocate(vm, Self);
        const out = obj.as(Self);
        out.* = Self{
            .obj = obj.*,
            .fields = Table(Value, Value).init(vm.allocator),
            .kind = kind,
        };
        return out;
    }

    pub fn destroy(self: *Self, vm: *VM) void {
        self.fields.deinit();
        vm.allocator.destroy(self);
    }

    pub fn equal(self: *Self, other: *Self) bool {
        if (self.fields.hm.count() != other.fields.hm.count()) return false;
        var it = self.fields.hm.iterator();
        while (it.next()) |kv|
            if (other.fields.get(kv.key_ptr.*)) |value| {
                if (!kv.value_ptr.*.equal(value)) return false;
            } else return false;

        return true;
    }

    pub fn get(self: *Self, index: Value, _: *VM) LoxError!Value {
        return self.fields.get(index) orelse Nil;
    }

    pub fn set(self: *Self, index: Value, value: Value, _: *VM) LoxError!void {
        _ = if (value.is(.Nil))
            self.fields.delete(index)
        else switch (self.kind) {
            .Table => try self.fields.set(index, value),
            .HashSet => try self.fields.set(index, index),
        };
    }
};

pub const Array = struct {
    const Self = @This();
    obj: Obj,
    values: ArrayList(Value),

    pub fn new(vm: *VM, capacity: u8) LoxError!*Self {
        const obj = try Obj.allocate(vm, Self);
        const out = obj.as(Self);
        out.* = Self{
            .obj = obj.*,
            .values = ArrayList(Value).initCapacity(vm.allocator, @max(4, @as(usize, @intCast(capacity)))) catch
                return LoxError.OutOfMemoryError,
        };
        return out;
    }

    pub fn destroy(self: *Self, vm: *VM) void {
        self.values.deinit();
        vm.allocator.destroy(self);
    }

    pub fn markValues(self: *Self, vm: *VM) !void {
        for (self.values.items) |value| {
            try value.mark(vm);
        }
    }

    pub fn equal(self: *Self, other: *Self) bool {
        if (self.values.items.len != other.values.items.len) return false;
        for (self.values.items, 0..) |item, i| {
            if (!item.equal(other.values.items[i])) return false;
        }
        return true;
    }

    fn checkIndex(self: *Self, index: Value, isSet: bool, vm: *VM) !usize {
        if (!index.is(.Number))
            return vm.runtimeError("Expected an integer argument", .{});

        const i = index.as(f64);

        if (i != @as(f64, @floatFromInt(@as(i64, @intFromFloat(i)))))
            return vm.runtimeError("Expected an integer argument", .{});

        const res: usize = @intFromFloat(i);
        if ((i < 0) or
            (!isSet and res >= self.values.items.len) or
            (isSet and !(res <= self.values.items.len)))
            return vm.runtimeError(
                "index out of bounds: index {d}, len {d}",
                .{ i, self.values.items.len },
            );
        return res;
    }

    pub fn get(self: *Self, index: Value, vm: *VM) LoxError!Value {
        const i = try self.checkIndex(index, false, vm);
        return self.values.items[i];
    }

    pub fn set(self: *Self, index: Value, value: Value, vm: *VM) LoxError!void {
        const i = try self.checkIndex(index, true, vm);
        if (i == self.values.items.len) {
            self.values.append(value) catch {
                eprint("OOME: can't allocate object", .{});
                return LoxError.OutOfMemoryError;
            };
        } else self.values.items[i] = value;
    }
};
