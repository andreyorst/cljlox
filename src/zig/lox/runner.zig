const std = @import("std");
const Allocator = std.mem.Allocator;

const value = @import("value.zig");
const Value = value.Value;
const Nil = value.Nil;
const VM = @import("vm.zig").VM;

const common = @import("common.zig");
const print = common.print;
const eprint = common.eprint;
const OPCode = common.OPCode;
const str = common.str;
const readFile = common.readFile;
const LoxError = common.LoxError;

pub fn repl(vm: *VM) LoxError!?Value {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const stdin = std.io.getStdIn();
    while (true) {
        print("ziglox> ", .{});
        var array_list = std.ArrayList(u8).init(allocator);
        defer array_list.deinit();
        stdin.reader().streamUntilDelimiter(array_list.writer(), '\n', 2048) catch return null;
        const line = array_list.toOwnedSlice() catch return null;
        if (vm.interpret(&line)) |val| {
            common.print("{}\n", .{val});
        } else |err| switch (err) {
            error.CompileError => eprint("Compile Error\n", .{}),
            error.RuntimeError => eprint("Runtime Error\n", .{}),
            error.OutOfMemoryError => return err,
        }
    }
}

pub fn file(path: str, vm: *VM) LoxError!?Value {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var source = readFile(path, allocator);
    defer allocator.free(source);
    return try vm.interpret(&source);
}
