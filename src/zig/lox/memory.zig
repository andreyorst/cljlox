const std = @import("std");
const Allocator = std.mem.Allocator;

const common = @import("common.zig");
const Value = @import("value.zig").Value;
const Table = @import("table.zig").Table;
const VM = @import("vm.zig").VM;
const Obj = @import("object.zig").Obj;
const Compiler = @import("compiler.zig").Compiler;

const GC_HEAP_GROW_FACTOR = 2;

pub const GCAllocator = struct {
    vm: ?*VM,
    parentAllocator: Allocator,
    bytesAllocated: usize,
    nextGC: usize,

    pub fn init(parentAllocator: Allocator) GCAllocator {
        return .{
            .vm = null,
            .parentAllocator = parentAllocator,
            .bytesAllocated = 0,
            .nextGC = 1024 * 1024,
        };
    }

    pub fn allocator(self: *GCAllocator) Allocator {
        return .{
            .ptr = self,
            .vtable = &.{
                .alloc = alloc,
                .resize = resize,
                .free = free,
            },
        };
    }

    fn alloc(ctx: *anyopaque, len: usize, log2_ptr_align: u8, ra: usize) ?[*]u8 {
        const self: *GCAllocator = @ptrCast(@alignCast(ctx));
        if ((self.bytesAllocated + len > self.nextGC) or common.DEBUG_STRESS_GC) {
            self.collectGarbage() catch return null;
        }

        const result = self.parentAllocator.rawAlloc(len, log2_ptr_align, ra);
        if (result != null) self.bytesAllocated += len;
        return result;
    }

    fn resize(ctx: *anyopaque, buf: []u8, log2_buf_align: u8, new_len: usize, ra: usize) bool {
        const self: *GCAllocator = @ptrCast(@alignCast(ctx));
        if (new_len > buf.len) {
            if ((self.bytesAllocated + (new_len - buf.len) > self.nextGC) or common.DEBUG_STRESS_GC) {
                self.collectGarbage() catch return false;
            }
        }
        if (self.parentAllocator.rawResize(buf, log2_buf_align, new_len, ra)) {
            if (new_len <= buf.len) {
                self.bytesAllocated -= buf.len - new_len;
            } else {
                self.bytesAllocated += new_len - buf.len;
            }

            return true;
        }
        std.debug.assert(new_len > buf.len);
        return false;
    }

    fn free(ctx: *anyopaque, buf: []u8, log2_buf_align: u8, ra: usize) void {
        const self: *GCAllocator = @ptrCast(@alignCast(ctx));
        self.parentAllocator.rawFree(buf, log2_buf_align, ra);
        self.bytesAllocated -= buf.len;
    }

    // Garbage collector implementation

    fn markCompilerRoots(self: *GCAllocator) !void {
        const vm = self.vm orelse return;
        if (vm.parser) |p| {
            var comp: ?*Compiler = p.compiler;
            while (comp) |c| {
                try c.function.obj.mark(vm);
                comp = c.enclosing;
            }
        }
    }

    fn markRoots(self: *GCAllocator) !void {
        const vm = self.vm orelse return;

        var slot: [*]Value = @ptrCast(vm.stack);
        while (@intFromPtr(slot) < @intFromPtr(vm.stackTop)) : (slot += 1) {
            try slot[0].mark(vm);
        }

        try vm.globals.mark(vm);

        var i: u32 = 0;
        while (i < vm.frameCount) : (i += 1) {
            try vm.frames[i].closure.obj.mark(vm);
        }

        var upvalue = vm.openUpvalues;
        while (upvalue) |u| {
            try u.obj.mark(vm);
            upvalue = u.next;
        }

        try self.markCompilerRoots();

        if (vm.initString) |s|
            try s.obj.mark(vm);
    }

    fn traceReferences(self: *GCAllocator) !void {
        const vm = self.vm orelse return;
        while (vm.grayStack.items.len > 0) {
            const object = vm.grayStack.pop();
            try object.blacken(vm);
        }
    }

    fn sweep(self: *GCAllocator) void {
        const vm = self.vm orelse return;
        var previous: ?*Obj = null;
        var object = vm.objects;
        while (object) |o| {
            if (o.isMarked) {
                o.isMarked = false;
                previous = o;
                object = o.next;
            } else {
                var unreached = o;
                object = o.next;
                if (previous) |p| {
                    p.next = object;
                } else {
                    vm.objects = object;
                }
                unreached.destroy(vm);
            }
        }
    }

    pub fn collectGarbage(self: *GCAllocator) !void {
        const vm = self.vm orelse return;
        const before = self.bytesAllocated;
        if (comptime common.DEBUG_LOG_GC) common.eprint("-- gc begin\n", .{});

        try self.markRoots();
        try self.traceReferences();
        vm.strings.removeWhite();
        self.sweep();

        self.nextGC = self.bytesAllocated * GC_HEAP_GROW_FACTOR;

        if (comptime common.DEBUG_LOG_GC) common.eprint(
            "-- gc end\ncollected {d} bytes (from {d} to {d}) next at {d}\n",
            .{ before - self.bytesAllocated, before, self.bytesAllocated, self.nextGC },
        );
    }
};
