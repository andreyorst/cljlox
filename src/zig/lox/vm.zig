const std = @import("std");
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;

const Chunk = @import("chunk.zig").Chunk;

const common = @import("common.zig");
const Result = common.Result;
const Code = common.Code;
const OPCode = common.OPCode;
const str = common.str;
const LoxError = common.LoxError;
const print = common.print;
const eprint = common.eprint;

const Value = @import("value.zig").Value;
const Nil = Value.Nil;
const True = Value.True;
const False = Value.False;

const compiler = @import("compiler.zig");
const compile = compiler.compile;
const Compiler = compiler.Compiler;
const Parser = compiler.Parser;

const Object = @import("object.zig");
const Obj = Object.Obj;
const String = Object.String;
const Function = Object.Function;
const NativeFn = Object.NativeFn;
const NativeFunction = Object.NativeFunction;
const Closure = Object.Closure;
const Upvalue = Object.Upvalue;
const Array = Object.Array;
const HashTable = Object.HashTable;

const Table = @import("table.zig").Table;
const GCAllocator = @import("memory.zig").GCAllocator;

const DEBUG_TRACE_EXECUTION = false;
const FRAMES_MAX = 64;
const STACK_MAX = FRAMES_MAX * 256;

fn clockNative(_: u8, _: [*]Value, _: *VM) !Value {
    return Value.from(@as(f64, @floatFromInt(std.time.milliTimestamp())) / 1000.0);
}

fn lengthNative(n: u8, args: [*]Value, vm: *VM) !Value {
    if (n > 0) {
        const s = args[0];
        if (s.is(.Array)) return Value.from(s.as(*Array).values.items.len);
        if (s.is(.String)) return Value.from(s.as(*String).bytes.len);
    }
    return vm.runtimeError("Expected a string as an argument.", .{});
}

fn tostringNative(nargs: u8, args: [*]Value, vm: *VM) !Value {
    if (nargs == 0) return vm.runtimeError("Expected an argument", .{});
    const arg = args[0];

    if (arg.is(.String)) return arg;

    const s = arg.tostring(vm.allocator) catch
        return vm.runtimeError("OOME: unable to create string", .{});
    defer vm.allocator.free(s);

    return (try String.copy(vm, s)).obj.asValue();
}

fn sleepNative(nargs: u8, args: [*]Value, vm: *VM) !Value {
    if (nargs == 0)
        return vm.runtimeError("Expected an argument", .{});

    const arg = args[0];
    if (!arg.is(.Number))
        return vm.runtimeError("Expected an integer argument", .{});

    const f = arg.as(f64);
    const i: i64 = @intFromFloat(f);
    if (f != @as(f64, @floatFromInt(i)))
        return vm.runtimeError("Expected an integer argument", .{});

    if (i > 0) std.time.sleep(@intCast(i * 1000000));
    return Nil;
}

fn printNative(nargs: u8, args: [*]Value, vm: *VM) !Value {
    for (0..nargs) |i| {
        if (i != 0) print(" ", .{});
        var array_list = std.ArrayList(u8).init(vm.allocator);
        defer array_list.deinit();

        args[i].format("{}", .{}, array_list.writer()) catch
            return vm.runtimeError("OOME: unable to format string", .{});

        var offset: u32 = 0;
        var escaped: bool = false;
        for (0..array_list.items.len) |j| {
            const char = array_list.items[j - offset];
            if (!escaped and char == '\\') {
                escaped = true;
                _ = array_list.orderedRemove(j - offset);
                offset += 1;
            } else {
                escaped = false;
            }
        }
        const s = array_list.toOwnedSlice() catch
            return vm.runtimeError("OOME: unable to format string", .{});
        defer vm.allocator.free(s);
        print("{s}", .{if (s[0] == '"') s[1 .. s.len - 1] else s});
    }
    print("\n", .{});
    return Nil;
}

fn typeNative(nargs: u8, args: [*]Value, vm: *VM) !Value {
    if (nargs == 0)
        return vm.runtimeError("Expected an argument", .{});

    return (try String.copy(vm, args[0].kind())).obj.asValue();
}

const CallFrame = struct {
    closure: *Closure,
    ip: [*]Code,
    slots: [*]Value,

    inline fn readByte(self: *CallFrame) Code {
        const byte = self.ip[0];
        self.ip += 1;
        return byte;
    }

    inline fn readShort(self: *CallFrame) u16 {
        const short = @as(u16, @intCast(self.ip[0])) << 8 | self.ip[1];
        self.ip += 2;
        return short;
    }

    inline fn readConstant(self: *CallFrame) Value {
        const byte = self.readByte();
        return self.closure.function.chunk.constants.items[byte];
    }
};

pub const VM = struct {
    stack: *[STACK_MAX]Value,
    stackTop: [*]Value,
    allocator: Allocator,
    objects: ?*Obj,
    strings: Table(*String, Value),
    globals: Table(*String, Value),
    frames: *[FRAMES_MAX]CallFrame,
    frameCount: u32,
    openUpvalues: ?*Upvalue,
    parser: ?*Parser,
    grayStack: ArrayList(*Obj),
    initString: ?*String,

    inline fn resetStack(self: *VM) void {
        self.stackTop = @ptrCast(self.stack);
        self.openUpvalues = null;
        self.frameCount = 0;
    }

    pub fn init(gcAllocator: Allocator) !*VM {
        const static = struct {
            var stack: [STACK_MAX]Value = [1]Value{Nil} ** STACK_MAX;
            var frames: [FRAMES_MAX]CallFrame = [1]CallFrame{undefined} ** FRAMES_MAX;
        };
        const gc: *GCAllocator = @ptrCast(@alignCast(gcAllocator.ptr));
        const vm = try gc.parentAllocator.create(VM);
        gc.vm = vm;
        vm.* = VM{
            .stack = &static.stack,
            .stackTop = undefined,
            .allocator = gcAllocator,
            .objects = null,
            .strings = Table(*String, Value).init(gcAllocator),
            .globals = Table(*String, Value).init(gcAllocator),
            .frames = &static.frames,
            .frameCount = 0,
            .openUpvalues = null,
            .parser = null,
            .grayStack = ArrayList(*Obj).init(gc.parentAllocator),
            .initString = null,
        };
        vm.resetStack();
        vm.initString = try String.copy(vm, "init");
        try vm.defineNative("clock", clockNative);
        try vm.defineNative("length", lengthNative);
        try vm.defineNative("tostring", tostringNative);
        try vm.defineNative("sleep", sleepNative);
        try vm.defineNative("print", printNative);
        try vm.defineNative("type", typeNative);
        return vm;
    }

    fn freeObjects(self: *VM) void {
        var object = self.objects;
        while (object) |o| {
            const next = o.next;
            o.destroy(self);
            object = next;
        }
    }

    pub fn deinit(self: *VM) void {
        const gc: *GCAllocator = @ptrCast(@alignCast(self.allocator.ptr));

        self.initString = null;
        self.strings.deinit();
        self.globals.deinit();
        self.freeObjects();
        self.grayStack.deinit();

        gc.parentAllocator.destroy(self);
        gc.vm = null;
    }

    pub inline fn push(self: *VM, value: Value) void {
        self.stackTop[0] = value;
        self.stackTop += 1;
    }

    pub inline fn pop(self: *VM) Value {
        self.stackTop -= 1;
        return self.stackTop[0];
    }

    inline fn peek(self: *VM, distance: u32) Value {
        return (self.stackTop - 1 - distance)[0];
    }

    pub fn runtimeError(self: *VM, comptime fmt: []const u8, args: anytype) LoxError {
        eprint(fmt, args);
        eprint("\n", .{});

        var i: i32 = @intCast(self.frameCount - 1);
        while (i >= 0) : (i -= 1) {
            const frame = &self.frames[@intCast(i)];
            const function = frame.closure.function;
            const offset: u32 = @intCast(@intFromPtr(frame.ip) -
                @intFromPtr(function.chunk.code.items.ptr));
            eprint("[line {d}] in {s}\n", .{
                function.chunk.getLine(offset),
                if (function.name) |name| name.bytes else "script",
            });
        }

        self.resetStack();
        return LoxError.RuntimeError;
    }

    fn concatenate(self: *VM) !void {
        const b = self.peek(0).as(*String);
        const a = self.peek(1).as(*String);
        const s = self.allocator.alloc(u8, a.bytes.len + b.bytes.len) catch {
            eprint("OOME: Can't allocate memory to concatenate strings.", .{});
            return LoxError.OutOfMemoryError;
        };
        std.mem.copy(u8, s, a.bytes);
        std.mem.copy(u8, s[a.bytes.len..], b.bytes);
        const res = try String.take(self, s);
        _ = self.pop();
        (self.stackTop - 1)[0] = res.obj.asValue();
    }

    fn call(self: *VM, closure: *Closure, argCount: u32) !void {
        if (argCount < closure.function.arity) {
            return self.runtimeError(
                "Expected {d} arguments but got {d}.",
                .{ closure.function.arity, argCount },
            );
        }

        if (self.frameCount == FRAMES_MAX) {
            return self.runtimeError("Stack overflow.", .{});
        }

        var frame = &self.frames[self.frameCount];
        frame.closure = closure;
        frame.ip = closure.function.chunk.code.items.ptr;
        frame.slots = self.stackTop - argCount - 1;
        self.frameCount += 1;
    }

    fn defineNative(self: *VM, name: str, function: NativeFn) !void {
        const s = try String.copy(self, name);
        self.push(s.obj.asValue());
        const nf = try NativeFunction.new(self, function, name);
        self.push(nf.obj.asValue());
        _ = try self.globals.set(self.stack[0].as(*String), self.stack[1]);
        _ = self.pop();
        _ = self.pop();
    }

    fn callValue(self: *VM, callee: Value, argCount: u8) !void {
        if (callee.is(.Obj)) switch (callee.as(*Obj).objType) {
            .Closure => return self.call(callee.as(*Closure), argCount),
            .NativeFunction => {
                const native = callee.as(*NativeFunction);
                const result = try native.function(argCount, self.stackTop - argCount, self);
                self.stackTop -= argCount + 1;
                self.push(result);
                return;
            },
            .HashTable, .Array => {
                self.stackTop -= argCount + 1;
                const result = try callee.get(self.stackTop[1], self);
                self.push(result);
                return;
            },
            else => {},
        };
        return self.runtimeError("Can only call functions and classes.", .{});
    }

    fn invoke(self: *VM, name: Value, argCount: u8) !void {
        const receiver = self.peek(argCount);
        if (!receiver.is(.HashTable))
            return self.runtimeError("Only tables have fields.", .{});
        const hash = receiver.as(*HashTable);
        const callee = hash.fields.get(name) orelse Nil;
        return self.callValue(callee, argCount);
    }

    fn captureUpvalue(self: *VM, local: *Value) LoxError!*Upvalue {
        var prevUpvalue: ?*Upvalue = null;
        var upvalue = self.openUpvalues;
        while (upvalue) |u| {
            if (@intFromPtr(u.location) <= @intFromPtr(local)) break;
            prevUpvalue = upvalue;
            upvalue = u.next;
        }

        if (upvalue) |u| if (u.location == local) return u;

        const createdUpvalue = try Upvalue.new(self, local);
        createdUpvalue.next = upvalue;

        if (prevUpvalue) |prev| {
            prev.next = createdUpvalue;
        } else {
            self.openUpvalues = createdUpvalue;
        }

        return createdUpvalue;
    }

    fn closeUpvalues(self: *VM, last: *Value) void {
        while (self.openUpvalues) |open| {
            if (@intFromPtr(open.location) < @intFromPtr(last)) break;
            const upvalue = open;
            upvalue.closed = upvalue.location.*;
            upvalue.location = &upvalue.closed;
            self.openUpvalues = upvalue.next;
        }
    }

    inline fn add(a: f64, b: f64) f64 {
        return a + b;
    }

    inline fn sub(a: f64, b: f64) f64 {
        return a - b;
    }

    inline fn div(a: f64, b: f64) f64 {
        return a / b;
    }

    inline fn mul(a: f64, b: f64) f64 {
        return a * b;
    }

    inline fn gt(a: f64, b: f64) bool {
        return a > b;
    }

    inline fn lt(a: f64, b: f64) bool {
        return a < b;
    }

    fn binaryOp(self: *VM, op: anytype) !void {
        if (!self.peek(0).is(.Number) or !self.peek(1).is(.Number))
            return self.runtimeError(
                "Operands must be numbers.",
                .{},
            );
        const b = self.pop().as(f64);
        (self.stackTop - 1)[0] = Value.from(op((self.stackTop - 1)[0].as(f64), b));
    }

    fn run(self: *VM) !Value {
        var frame = &self.frames[self.frameCount - 1];
        while (true) {
            if (DEBUG_TRACE_EXECUTION) {
                eprint("              ", .{});
                var sp: u32 = 0;
                while (&self.stack[sp] != &self.stackTop[0]) : (sp += 1) {
                    eprint("[ {} ]", .{self.stack[sp]});
                }
                eprint("\n", .{});
                const offset: u32 = @intCast(@intFromPtr(frame.ip) -
                    @intFromPtr(frame.closure.function.chunk.code.items.ptr));
                _ = frame.closure.function.chunk.disassembleInstruction(offset);
            }
            const op: OPCode = @enumFromInt(frame.readByte());
            switch (op) {
                .Constant => {
                    const constant = frame.readByte();
                    self.push(frame.closure.function.chunk.constants.items[constant]);
                },
                .Nil => self.push(Nil),
                .True => self.push(True),
                .False => self.push(False),
                .Pop => _ = self.pop(),
                .GetLocal => {
                    const slot = frame.readByte();
                    self.push(frame.slots[slot]);
                },
                .SetLocal => {
                    const slot = frame.readByte();
                    frame.slots[slot] = self.peek(0);
                },
                .GetGlobal => {
                    const name = frame.readConstant().as(*String);
                    if (self.globals.get(name)) |value| {
                        self.push(value);
                    } else {
                        return self.runtimeError("Undefined variable '{s}'.", .{name.bytes});
                    }
                },
                .DefineGlobal => {
                    const name = frame.readConstant().as(*String);
                    _ = try self.globals.set(name, self.peek(0));
                    _ = self.pop();
                },
                .SetGlobal => {
                    const name = frame.readConstant().as(*String);
                    if (try self.globals.set(name, self.peek(0))) {
                        _ = self.globals.delete(name);
                        return self.runtimeError("Undefined variable '{s}'.", .{name.bytes});
                    }
                },
                .Equal => {
                    const b = self.pop();
                    (self.stackTop - 1)[0] = (Value.from((self.stackTop - 1)[0].equal(b)));
                },
                .Greater => try self.binaryOp(gt),
                .Less => try self.binaryOp(lt),
                .Add => try if (self.peek(0).is(.String) and self.peek(1).is(.String))
                    self.concatenate()
                else
                    self.binaryOp(add),
                .Sub => _ = try self.binaryOp(sub),
                .Mul => _ = try self.binaryOp(mul),
                .Div => _ = try self.binaryOp(div),
                .Not => (self.stackTop - 1)[0] = Value.from((self.stackTop - 1)[0].isFalsey()),
                .Negate => if (!self.peek(0).is(.Number)) {
                    return self.runtimeError("Operands must be numbers.", .{});
                } else {
                    (self.stackTop - 1)[0] = Value.from(-(self.stackTop - 1)[0].as(f64));
                },
                .Jump => {
                    const offset = frame.readShort();
                    frame.ip += offset;
                },
                .JumpIfFalse => {
                    const offset = frame.readShort();
                    if (self.peek(0).isFalsey()) frame.ip += offset;
                },
                .Loop => {
                    const offset = frame.readShort();
                    frame.ip -= offset;
                },
                .Call => {
                    const argCount = frame.readByte();
                    _ = try self.callValue(self.peek(argCount), argCount);
                    frame = &self.frames[self.frameCount - 1];
                },
                .Closure => {
                    const function = frame.readConstant().as(*Function);
                    const closure = try Closure.new(self, function);
                    self.push(closure.obj.asValue());
                    for (closure.upvalues) |*upvalue| {
                        const isLocal = frame.readByte();
                        const index = frame.readByte();
                        if (isLocal != 0) {
                            upvalue.* = try self.captureUpvalue(&frame.slots[index]);
                        } else {
                            upvalue.* = frame.closure.upvalues[index];
                        }
                    }
                },
                .GetUpvalue => {
                    const slot = frame.readByte();
                    const value = frame.closure.upvalues[slot].?.location.*;
                    self.push(value);
                },
                .SetUpvalue => {
                    const slot = frame.readByte();
                    frame.closure.upvalues[slot].?.location.* = self.peek(0);
                },
                .CloseUpvalue => {
                    self.closeUpvalues(@ptrCast(self.stackTop - 1));
                    _ = self.pop();
                },
                .Table => {
                    const table = frame.readConstant().as(*HashTable);
                    const valueCount = frame.readByte();
                    var i = valueCount;
                    while (i > 0) : (i -= 2) {
                        const slot = (self.stackTop - i);
                        if (!(table.fields.set(slot[0], slot[1]) catch
                            return self.runtimeError("OOME: can't append to table", .{})))
                            return self.runtimeError("duplicate key: {}", .{slot[0]});
                    }
                    self.stackTop -= valueCount;
                    self.push(table.obj.asValue());
                },
                .HashSet => {
                    const table = frame.readConstant().as(*HashTable);
                    const valueCount = frame.readByte();
                    var i = valueCount;
                    while (i > 0) : (i -= 1) {
                        const slot = (self.stackTop - i);
                        if (!(table.fields.set(slot[0], slot[0]) catch
                            return self.runtimeError("OOME: can't append to hash set", .{})))
                            return self.runtimeError("duplicate key: {}", .{slot[0]});
                    }
                    self.stackTop -= valueCount;
                    self.push(table.obj.asValue());
                },
                .GetProperty => {
                    if (!self.peek(0).is(.HashTable))
                        return self.runtimeError("Only tables have fields.", .{});

                    const hash = self.peek(0).as(*HashTable);
                    const name = frame.readConstant();

                    const value = try hash.get(name, self);

                    (self.stackTop - 1)[0] = value;
                },
                .SetProperty => {
                    if (!self.peek(1).is(.HashTable))
                        return self.runtimeError("Only tables have fields.", .{});

                    const hash = self.peek(1).as(*HashTable);
                    try hash.set(frame.readConstant(), self.peek(0), self);
                    const value = self.pop();
                    (self.stackTop - 1)[0] = value;
                },
                .Array => {
                    const arr = frame.readConstant().as(*Array);
                    const valueCount = frame.readByte();
                    for (0..valueCount) |i| {
                        arr.values.append((self.stackTop - (valueCount - i))[0]) catch
                            return self.runtimeError("OOME: can't append to array", .{});
                    }
                    self.stackTop -= valueCount;
                    self.push(arr.obj.asValue());
                },
                .GetIndex => {
                    const index = self.pop();
                    (self.stackTop - 1)[0] = try self.peek(0).get(index, self);
                },
                .SetIndex => {
                    const value = self.pop();
                    const index = self.pop();
                    try self.peek(0).set(index, value, self);
                },
                .Invoke => {
                    const method = frame.readConstant();
                    const argCount = frame.readByte();
                    try self.invoke(method, argCount);
                    frame = &self.frames[self.frameCount - 1];
                },
                .InvokeFromArray => {
                    const argCount = frame.readByte();
                    const index = self.peek(argCount);
                    const obj = self.peek(argCount + 1);
                    const callee = try obj.get(index, self);
                    try self.callValue(callee, argCount);
                    frame = &self.frames[self.frameCount - 1];
                },
                .Ret => {
                    const result = self.pop();
                    self.closeUpvalues(&frame.slots[0]);
                    self.frameCount -= 1;

                    if (self.frameCount == 0) {
                        _ = self.pop();
                        return result;
                    }

                    self.stackTop = frame.slots;
                    self.push(result);
                    frame = &self.frames[self.frameCount - 1];
                },
            }
        }
    }

    pub fn interpret(self: *VM, source: *const str) !Value {
        if (try compile(source, self)) |function| {
            self.push(function.obj.asValue());
            const closure = try Closure.new(self, function);
            _ = self.pop();
            self.push(closure.obj.asValue());
            _ = try self.call(closure, 0);

            const result = try self.run();
            return result;
        }
        return error.CompileError;
    }
};
