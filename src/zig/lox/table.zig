const std = @import("std");
const HashMap = std.hash_map.HashMap;
const ArrayList = std.ArrayList;
const Allocator = std.mem.Allocator;

const common = @import("common.zig");
const str = common.str;
const LoxError = common.LoxError;

const values = @import("value.zig");
const Value = values.Value;
const Nil = values.Nil;
const True = values.True;

const VM = @import("vm.zig").VM;

const object = @import("object.zig");
const String = object.String;

const table_max_load = 75;

pub fn Table(comptime KeyType: type, comptime ValueType: type) type {
    return struct {
        hm: HashMap(KeyType, ValueType, Ctx, table_max_load),

        const Ctx = struct {
            pub fn hash(_: Ctx, key: KeyType) u64 {
                return @intCast(key.getHash());
            }

            pub fn eql(_: Ctx, a: KeyType, b: KeyType) bool {
                return a.getHash() == b.getHash() and a.equal(b);
            }
        };

        pub fn init(allocator: Allocator) @This() {
            return .{ .hm = HashMap(KeyType, ValueType, Ctx, table_max_load).init(allocator) };
        }

        pub fn deinit(self: *@This()) void {
            self.hm.deinit();
        }

        pub fn findString(self: *@This(), chars: str, hash: u32) ?*String {
            var s = String{ .obj = undefined, .hash = hash, .bytes = chars };
            return self.hm.getKey(&s);
        }

        pub fn set(self: *@This(), key: KeyType, value: ValueType) LoxError!bool {
            return if (self.hm.fetchPut(key, value) catch {
                common.eprint("OOME: can't put the key to a table", .{});
                return LoxError.OutOfMemoryError;
            }) |_|
                false // key is not new
            else
                true;
        }

        pub fn get(self: *@This(), key: KeyType) ?ValueType {
            return self.hm.get(key);
        }

        pub fn delete(self: *@This(), key: KeyType) bool {
            return self.hm.remove(key);
        }

        pub fn removeWhite(self: *@This()) void {
            var it = self.hm.iterator();
            while (it.next()) |kv| {
                if (!kv.key_ptr.*.isMarked()) {
                    _ = self.delete(kv.key_ptr.*);
                }
            }
        }

        pub fn mark(self: *@This(), vm: *VM) !void {
            var it = self.hm.iterator();
            while (it.next()) |kv| {
                try kv.key_ptr.*.mark(vm);
                try kv.value_ptr.*.mark(vm);
            }
        }
    };
}
