const std = @import("std");
const ArrayList = std.ArrayList;
const Allocator = std.mem.Allocator;

pub const str = []const u8;
pub const Code = u8;

pub const DEBUG_LOG_GC = false;
pub const DEBUG_STRESS_GC = false;

pub const OPCode = enum(Code) {
    Constant,
    Add,
    Sub,
    Mul,
    Div,
    Negate,
    Ret,
    Nil,
    True,
    False,
    Not,
    Equal,
    Greater,
    Less,
    Pop,
    DefineGlobal,
    GetGlobal,
    SetGlobal,
    GetLocal,
    SetLocal,
    JumpIfFalse,
    Jump,
    Loop,
    Call,
    Closure,
    GetUpvalue,
    SetUpvalue,
    CloseUpvalue,
    Table,
    GetProperty,
    SetProperty,
    Invoke,
    Array,
    SetIndex,
    GetIndex,
    InvokeFromArray,
    HashSet,
};

pub fn exit(code: u8, comptime fmt: []const u8, args: anytype) noreturn {
    eprint(fmt, args);
    std.process.exit(code);
}

pub fn print(comptime fmt: []const u8, args: anytype) void {
    const out = std.io.getStdOut().writer();
    out.print(fmt, args) catch |e|
        exit(100, "error writing to stdout: {}", .{e});
}

pub fn eprint(comptime fmt: []const u8, args: anytype) void {
    const out = std.io.getStdErr().writer();
    out.print(fmt, args) catch |e|
        exit(100, "error writing to stderr: {}", .{e});
}

pub fn readFile(path: str, allocator: Allocator) []u8 {
    var file = std.fs.cwd().openFile(path, .{}) catch
        exit(74, "Could not open file {s}", .{path});
    defer file.close();

    var reader = std.io.bufferedReader(file.reader());
    var stream = reader.reader();

    return stream.readAllAlloc(allocator, std.math.maxInt(u32)) catch
        exit(74, "Not enough memory to read file {s}", .{path});
}

pub fn typeNameUnqualified(comptime T: type) []const u8 {
    const name = @typeName(T);
    return name[if (std.mem.lastIndexOfAny(u8, name, ".")) |i| i + 1 else 0..];
}

pub const LoxError = error{
    OutOfMemoryError,
    RuntimeError,
    CompileError,
};
