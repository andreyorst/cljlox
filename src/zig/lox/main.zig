const std = @import("std");
const common = @import("common.zig");
const exit = common.exit;
const VM = @import("vm.zig").VM;
const run = @import("runner.zig");
const print = common.print;
const GCAllocator = @import("memory.zig").GCAllocator;

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const arena_allocator = arena.allocator();
    var gc = GCAllocator.init(arena_allocator);
    const allocator = gc.allocator();

    var args = std.process.args();
    var name = args.next() orelse "ziglox";

    var vm = try VM.init(allocator);
    defer vm.deinit();

    const maybeRes = switch (args.inner.count) {
        1 => run.repl(vm),
        2 => run.file(args.next() orelse unreachable, vm),
        else => exit(64, "Usage: {s} [path]\n", .{name}),
    } catch |err| switch (err) {
        error.CompileError => exit(75, "Compile Error\n", .{}),
        error.RuntimeError => exit(85, "Runtime Error\n", .{}),
        error.OutOfMemoryError => exit(74, "Out of memory error", .{}),
    };
    if (maybeRes) |res| print("{}\n", .{res});
}
