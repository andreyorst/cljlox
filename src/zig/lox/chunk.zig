const std = @import("std");
const ArrayList = std.ArrayList;
const Allocator = std.mem.Allocator;

const common = @import("common.zig");
const Code = common.Code;
const OPCode = common.OPCode;
const exit = common.exit;
const print = common.print;
const eprint = common.print;
const str = common.str;
const Upcalue = common.Upvalue;
const LoxError = common.LoxError;

const Value = @import("value.zig").Value;

const Object = @import("object.zig");
const Function = Object.Function;

const VM = @import("vm.zig").VM;

pub const Chunk = struct {
    const Self = @This();
    code: ArrayList(Code),
    constants: ArrayList(Value),
    lines: ArrayList(u32),

    pub fn init(allocator: Allocator) Chunk {
        return Chunk{
            .code = ArrayList(Code).init(allocator),
            .constants = ArrayList(Value).init(allocator),
            .lines = ArrayList(u32).init(allocator),
        };
    }

    pub fn deinit(self: *Self) void {
        self.code.deinit();
        self.constants.deinit();
        self.lines.deinit();
    }

    fn writeLine(self: *Self, line: u32) LoxError!void {
        const len = self.lines.items.len;
        if (len > 1 and self.lines.items[len - 2] == line) {
            self.lines.items[len - 1] = self.lines.items[len - 1] + 1;
        } else {
            self.lines.append(line) catch {
                eprint("OOME: can't allocate memory for line information", .{});
                return LoxError.OutOfMemoryError;
            };

            self.lines.append(1) catch {
                eprint("OOME: can't allocate memory for line information", .{});
                return LoxError.OutOfMemoryError;
            };
        }
    }

    pub fn write(self: *Self, byte: Code, line: u32) LoxError!void {
        self.code.append(byte) catch {
            eprint("OOME: can't allocate memory for the instruction", .{});
            return LoxError.OutOfMemoryError;
        };
        try self.writeLine(line);
    }

    pub fn addConstant(self: *Self, value: Value) LoxError!Code {
        self.constants.append(value) catch {
            eprint("OOME: can't allocate memory for the constant", .{});
            return LoxError.OutOfMemoryError;
        };
        return @truncate(self.constants.items.len - 1);
    }

    // Disassembling

    pub fn getLine(self: *const Self, offset: u32) u32 {
        var cnt: u32 = 0;
        var off: u32 = offset;
        while (off > 0) loop: {
            var n: u32 = self.lines.items[cnt + 1];
            while (n > 0) {
                if (off == 0)
                    break :loop;
                off = off - 1;
                n = n - 1;
            }
            cnt = cnt + 2;
        }
        return self.lines.items[cnt];
    }

    fn constantInstruction(self: *const Self, name: str, offset: u32) u32 {
        const constant = self.code.items[offset + 1];
        eprint("{s: <16} {d:>08} '{}'\n", .{ name, constant, self.constants.items[constant] });
        return offset + 2;
    }

    fn simpleInstruction(name: str, offset: u32) u32 {
        eprint("{s: <16}\n", .{name});
        return offset + 1;
    }

    fn byteInstruction(self: *const Self, name: str, offset: u32) u32 {
        var slot: u32 = 0;
        slot = @intCast(self.code.items[offset + 1]);
        eprint("{s: <16} {d:>08}\n", .{ name, slot });
        return offset + 2;
    }

    fn jumpInstruction(self: *const Self, name: str, sign: i32, offset: u32) u32 {
        const jump: u16 = (@as(u16, self.code.items[offset + 2]) << 0) |
            (@as(u16, self.code.items[offset + 1]) << 8);
        eprint("{s: <16} {d:>04} -> {d}\n", .{ name, offset, @as(i32, @intCast(offset)) + 3 + sign * jump });
        return offset + 3;
    }

    fn closureInstruction(self: *const Self, name: str, offset: u32) u32 {
        var off = offset + 1;
        const constant: u32 = @intCast(self.code.items[off]);
        off += 1;
        eprint("{s: <16} {d:>08} ", .{ name, constant });
        const value = self.constants.items[constant];
        eprint("{}\n", .{value});
        const function = value.as(*Function);
        var j: u32 = 0;
        while (j < function.upvalueCount) : (j += 1) {
            const isLocal = self.code.items[off] != 0;
            off += 1;
            const index = self.code.items[off];
            off += 1;
            print(
                "{d:0>04}          |                         {s} {d}\n",
                .{ off - 2, if (isLocal) "local" else "upvalue", index },
            );
        }
        return off;
    }

    fn invokeInstruction(self: *const Self, name: str, offset: u32) u32 {
        const constant = self.code.items[offset + 1];
        const argCount = self.code.items[offset + 2];
        eprint(
            "{s: <16} ({d} args) {d:>04} '{}'\n",
            .{ name, argCount, constant, self.constants.items[constant] },
        );
        return offset + 3;
    }

    fn arrayInstruction(self: *const Self, name: str, offset: u32) u32 {
        const constant = self.code.items[offset + 1];
        const nvalues = self.code.items[offset + 2];
        eprint("{s: <16} {d:>08} '{}' {}\n", .{ name, constant, self.constants.items[constant], nvalues });
        return offset + 3;
    }

    pub fn disassembleInstruction(self: *const Self, offset: u32) u32 {
        eprint("{d:0>4} ", .{offset});
        if (offset > 0 and self.getLine(offset) == self.getLine(offset - 1)) {
            eprint("       | ", .{});
        } else {
            eprint("{d:>08} ", .{self.getLine(offset)});
        }
        var opcode = self.code.items[offset];
        return switch (@as(OPCode, @enumFromInt(opcode))) {
            .Ret,
            .Negate,
            .Add,
            .Sub,
            .Mul,
            .Div,
            .Nil,
            .True,
            .False,
            .Equal,
            .Greater,
            .Less,
            .Not,
            .Pop,
            .CloseUpvalue,
            .Inherit,
            .GetIndex,
            .SetIndex,
            => |op| simpleInstruction(@tagName(op), offset),
            .Constant,
            .DefineGlobal,
            .GetGlobal,
            .SetGlobal,
            .Class,
            .GetProperty,
            .SetProperty,
            .Method,
            .GetSuper,
            => |op| self.constantInstruction(@tagName(op), offset),
            .GetLocal,
            .SetLocal,
            .Call,
            .GetUpvalue,
            .SetUpvalue,
            .InvokeFromArray,
            => |op| self.byteInstruction(@tagName(op), offset),
            .Jump,
            .JumpIfFalse,
            .Loop,
            => |op| self.jumpInstruction(@tagName(op), 1, offset),
            .Closure => |op| self.closureInstruction(@tagName(op), offset),
            .Invoke,
            .SuperInvoke,
            => |op| self.invokeInstruction(@tagName(op), offset),
            .Array => |op| self.arrayInstruction(@tagName(op), offset),
        };
    }

    pub fn disassemble(self: *const Self, name: str) void {
        print("==== {s} ====\n", .{name});
        var offset: u32 = 0;
        while (offset < self.code.items.len) {
            offset = self.disassembleInstruction(offset);
        }
    }
};
