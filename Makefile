PATCH := $(strip $(shell git -C ./ rev-list HEAD --count))
CLJ_SOURCES := $(shell find ./src/clojure -type f -name '*.clj')
ZIG_INSTALLATION ?= ~/.zig
ZIG_STD ?= $(ZIG_INSTALLATION)/lib/std
ZIG_STD_SOURCES := $(shell find $(ZIG_STD) -type f -name '*.zig')
ZIG_SOURCES := $(shell find ./src/zig -type f -name '*.zig')
ZIG_OPTIMIZE ?= Debug
ZIG ?= $(ZIG_INSTALLATION)/zig

.PHONY: cljlox run_cljlox test_cljlox ziglox run_ziglox test_ziglox release_ziglox clean distclean help

cljlox: target/clojure/lox-0.0.$(PATCH)-standalone.jar

target/clojure/lox-0.0.$(PATCH)-standalone.jar: $(CLJ_SOURCES)
	clojure -T:build uber
	@echo "#!/bin/sh" > cljlox
	@echo "java -jar target/clojure/lox-0.0.$(PATCH)-standalone.jar \$$@" >> cljlox
	@chmod +x cljlox

run_cljlox: cljlox
	./cljlox

test_cljlox:
	clojure -M:test/kaocha

ziglox: ./target/zig/bin/lox-0.1.$(PATCH)

./target/zig/bin/lox-0.1.$(PATCH): $(ZIG_SOURCES)
	$(ZIG) build -Doptimize=$(ZIG_OPTIMIZE) -p ./target/zig --cache-dir ./target/zig/.cache
	@ln -sf ./target/zig/bin/lox-0.1.$(PATCH) ziglox

run_ziglox: ziglox
	./ziglox

test_ziglox:
	$(ZIG) build -p ./target/zig --cache-dir ./target/zig/.cache test

release_ziglox: ZIG_OPTIMIZE=ReleaseFast
release_ziglox: | clean ziglox

tags:
	etags --language=none --regex=@zig.etags $(ZIG_SOURCES) $(ZIG_STD_SOURCES)

clean:
	rm -rf ./target
	rm -rf ./ziglox
	rm -rf ./cljlox

distclean:
	git clean -xdf

help:
	@echo "make cljlox         -- build CljLox interpreter" >&2
	@echo "make run_cljlox     -- (build and) run CljLox interpreter" >&2
	@echo "make test_cljlox    -- run CljLox tests" >&2
	@echo "make ziglox         -- build ZigLox VM" >&2
	@echo "make release_ziglox -- clear and build a release optimized ZigLox VM" >&2
	@echo "make run_ziglox     -- (build and) run ZigLox VM" >&2
	@echo "make test_ziglox    -- run ZigLox tests" >&2
	@echo "make clean          -- remove the target dir" >&2
	@echo "make distclean      -- remove all files that don't belong to the repo" >&2
	@echo "make help           -- print this message and exit" >&2
