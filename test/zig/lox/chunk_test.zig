const t = @import("std").testing;
const Chunk = @import("src/chunk.zig").Chunk;

const Value = @import("src/value.zig").Value;

const common = @import("src/common.zig");
const OPCode = common.OPCode;
const Code = common.Code;

test "basic chunk" {
    var chunk = Chunk.init(t.allocator);
    defer chunk.deinit();

    var n: usize = 0;
    const a = Value.from(10.0);
    const b = Value.from(20.0);

    n = try chunk.addConstant(a);
    try chunk.write(@intFromEnum(OPCode.Constant), 123);
    try chunk.write(@truncate(n), 123);
    n = try chunk.addConstant(b);
    try chunk.write(@intFromEnum(OPCode.Constant), 123);
    try chunk.write(@truncate(n), 123);
    try chunk.write(@intFromEnum(OPCode.Ret), 124);

    try t.expectEqualSlices(
        Code,
        chunk.code.items,
        &[5]Code{
            @intFromEnum(OPCode.Constant),
            0,
            @intFromEnum(OPCode.Constant),
            1,
            @intFromEnum(OPCode.Ret),
        },
    );
    try t.expectEqualSlices(
        Value,
        chunk.constants.items,
        &[2]Value{ a, b },
    );
}
