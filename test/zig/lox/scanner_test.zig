const std = @import("std");
const t = @import("std").testing;
const ArrayList = std.ArrayList;
const Tuple = std.meta.Tuple;
const common = @import("src/common.zig");
const str = common.str;
const scan = @import("src/scanner.zig");
const Scanner = scan.Scanner;
const TokenType = scan.TokenType;

test "expr_scanning" {
    const cases = [_]Tuple(&.{ str, []const TokenType }){
        .{
            "if (n > 0) return true",
            &[8]TokenType{ .IF, .LEFT_PAREN, .IDENTIFIER, .GREATER, .NUMBER, .RIGHT_PAREN, .RETURN, .TRUE },
        },
        .{
            "-1 - 1",
            &[4]TokenType{ .MINUS, .NUMBER, .MINUS, .NUMBER },
        },
        .{
            "// comment\nx\n//comment",
            &[1]TokenType{.IDENTIFIER},
        },
    };
    for (cases) |case| {
        var scanner = Scanner.init(&case[0]);
        var tokens = ArrayList(TokenType).init(t.allocator);
        defer tokens.deinit();
        while (true) {
            const token = scanner.scan();
            switch (token.Type) {
                .EOF => break,
                else => try tokens.append(token.Type),
            }
        }
        try t.expectEqualSlices(
            TokenType,
            case[1],
            tokens.items,
        );
    }
}

test "tokens_scanning" {
    const cases = [_]Tuple(&.{ str, TokenType }){
        .{ "(", .LEFT_PAREN },
        .{ ")", .RIGHT_PAREN },
        .{ "{", .LEFT_BRACE },
        .{ "}", .RIGHT_BRACE },
        .{ ",", .COMMA },
        .{ ".", .DOT },
        .{ "-", .MINUS },
        .{ "+", .PLUS },
        .{ ";", .SEMICOLON },
        .{ "/", .SLASH },
        .{ "*", .STAR },
        .{ "!", .BANG },
        .{ "!=", .BANG_EQUAL },
        .{ "=", .EQUAL },
        .{ "==", .EQUAL_EQUAL },
        .{ ">", .GREATER },
        .{ ">=", .GREATER_EQUAL },
        .{ "<", .LESS },
        .{ "<=", .LESS_EQUAL },
        .{ "x", .IDENTIFIER },
        .{ "\"foo\"", .STRING },
        .{ "1234", .NUMBER },
        .{ "and", .AND },
        .{ "else", .ELSE },
        .{ "false", .FALSE },
        .{ "for", .FOR },
        .{ "fun", .FUN },
        .{ "if", .IF },
        .{ "nil", .NIL },
        .{ "or", .OR },
        .{ "return", .RETURN },
        .{ "true", .TRUE },
        .{ "var", .VAR },
        .{ "while", .WHILE },
    };

    for (cases) |case| {
        var scanner = Scanner.init(&case[0]);
        try t.expectEqual(case[1], scanner.scan().Type);
    }
}
