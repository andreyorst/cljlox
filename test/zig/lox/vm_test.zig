const std = @import("std");
const t = std.testing;

const Chunk = @import("src/chunk.zig").Chunk;
const VM = @import("src/vm.zig").VM;
const GCAllocator = @import("src/memory.zig").GCAllocator;
const run = @import("src/runner.zig");

const common = @import("src/common.zig");
const OPCode = common.OPCode;
const Code = common.Code;
const str = common.str;
const readFile = common.readFile;

const Value = @import("src/value.zig").Value;
const True = Value.True;
const False = Value.False;
const Nil = Value.Nil;

const object = @import("src/object.zig");
const Obj = object.Obj;

test "arithmetic test" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const source: str = "return - (1.2 + 3.8);";
    try t.expectEqual(Value.from(-5), try vm.interpret(&source));
}

test "logic test" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: Value,
    };

    const cases = [_]Case{
        .{ .expr = "return 1 == 1;", .expt = True },
        .{ .expr = "return 1 == 2;", .expt = False },

        .{ .expr = "return 1 < 2;", .expt = True },
        .{ .expr = "return 1 <= 2;", .expt = True },
        .{ .expr = "return 1 <= 1;", .expt = True },
        .{ .expr = "return 1 <= 0;", .expt = False },
        .{ .expr = "return 1 < 1;", .expt = False },

        .{ .expr = "return 2 > 1;", .expt = True },
        .{ .expr = "return 2 >= 1;", .expt = True },
        .{ .expr = "return 1 >= 1;", .expt = True },
        .{ .expr = "return 0 >= 1;", .expt = False },
        .{ .expr = "return 1 > 1;", .expt = False },

        .{ .expr = "return !1;", .expt = False },
        .{ .expr = "return !nil;", .expt = True },
        .{ .expr = "return !false;", .expt = True },
        .{ .expr = "return !true;", .expt = False },

        .{ .expr = "return \"foo\" == \"foo\";", .expt = True },
        .{ .expr = "return \"foo\" == \"fooo\";", .expt = False },
        .{ .expr = "return \"foo\" == \"bar\";", .expt = False },
        .{ .expr = "return 123 == \"bar\";", .expt = False },
    };

    for (cases) |case| {
        t.expectEqual(case.expt, try vm.interpret(&case.expr)) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "string test" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: str,
    };

    const cases = [_]Case{
        .{ .expr = "return \"foo\";", .expt = "foo" },
        .{ .expr = "return \"foo\" + \"bar\";", .expt = "foobar" },
        .{ .expr = "return \"foo\" + \"bar\" + \"baz\";", .expt = "foobarbaz" },
    };

    for (cases) |case| {
        const res = try vm.interpret(&case.expr);
        t.expectEqualSlices(
            u8,
            case.expt,
            res.as(*Obj).as(object.String).bytes,
        ) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "tostring test" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: str,
    };

    const cases = [_]Case{
        .{ .expr = "return tostring(\"foo\");", .expt = "foo" },
        .{ .expr = "return tostring(1);", .expt = "1" },
        .{ .expr = "return tostring(1.2);", .expt = "1.2" },
        .{ .expr = "return tostring(true);", .expt = "true" },
        .{ .expr = "return tostring(false);", .expt = "false" },
        .{ .expr = "return tostring(nil);", .expt = "nil" },
        .{
            .expr =
            \\ return tostring(["foo", "bar"]);
            ,
            .expt =
            \\[\\"foo\\", \\"bar\\"]
            ,
        },
        .{
            .expr = "return tostring(#{.foo => 1});",
            .expt =
            \\#{\\"foo\\" => 1}
            ,
        },
    };

    for (cases) |case| {
        const res = try vm.interpret(&case.expr);
        t.expectEqualSlices(
            u8,
            case.expt,
            res.as(*Obj).as(object.String).bytes,
        ) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "if" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: Value,
    };

    const cases = [_]Case{
        .{ .expr = "if (true) return 0;", .expt = Value.from(0) },
        .{ .expr = "if (true) return 1; else return 2;", .expt = Value.from(1) },
        .{ .expr = "if (false) return 3; else return 4;", .expt = Value.from(4) },
        .{ .expr = "if (nil) return 5; else return 6;", .expt = Value.from(6) },
        .{ .expr = "if (1 == 2) return 7; else return 8;", .expt = Value.from(8) },
    };

    for (cases) |case| {
        t.expectEqual(case.expt, try vm.interpret(&case.expr)) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "while" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: Value,
    };

    const cases = [_]Case{
        .{
            .expr = "var res = 0; while (res < 10) { res = res + 1; } return res;",
            .expt = Value.from(10),
        },
    };

    for (cases) |case| {
        t.expectEqual(case.expt, try vm.interpret(&case.expr)) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "for" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: Value,
    };

    const cases = [_]Case{
        .{
            .expr = "var res = 0; for (var i = 0; i < 10; i = i + 1) { res = res + i; } return res;",
            .expt = Value.from(45),
        },
    };

    for (cases) |case| {
        t.expectEqual(case.expt, try vm.interpret(&case.expr)) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "functions" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: Value,
    };

    const cases = [_]Case{
        .{ .expr = "fun a() {} return a();", .expt = Nil },
        .{ .expr = "fun b() {return a();} return b();", .expt = Nil },
        .{ .expr = "fun a() {return 10;} return a();", .expt = Value.from(10) },
        .{ .expr = "fun b() {return a() + 10;} return b();", .expt = Value.from(20) },
        .{ .expr = "fun fib(n) { if (n < 2) return n; return fib(n - 2) + fib(n - 1); } return fib(11);", .expt = Value.from(89) },
    };

    for (cases) |case| {
        t.expectEqual(case.expt, try vm.interpret(&case.expr)) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "anonymous functions" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: Value,
    };

    const cases = [_]Case{
        .{ .expr = "return fun () {return 1;}();", .expt = Value.from(1) },
        .{ .expr = "var f = fun () {return 2;}; return f();", .expt = Value.from(2) },
        .{
            .expr =
            \\ var recurse = fun (x) {
            \\   if (x < 10) return recurse(x+1);
            \\   return x;
            \\ };
            \\ return recurse(0);
            ,
            .expt = Value.from(10),
        },
    };

    for (cases) |case| {
        t.expectEqual(case.expt, try vm.interpret(&case.expr)) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "running files" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const result = try run.file("test/data/fib.lox", vm);
    try t.expectEqual(Value.from(21), result.?);
}

test "closures" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        file: str,
        expt: Value,
    };

    const cases = [_]Case{
        .{ .file = "test/data/closure-1.lox", .expt = True },
        .{ .file = "test/data/closure-2.lox", .expt = True },
        .{ .file = "test/data/closure-3.lox", .expt = True },
        .{ .file = "test/data/closure-4.lox", .expt = True },
        .{ .file = "test/data/closure-5.lox", .expt = True },
        .{ .file = "test/data/closure-6.lox", .expt = True },
        .{ .file = "test/data/vec.lox", .expt = Value.from(42) },
        .{ .file = "test/data/linked-list.lox", .expt = True },
    };

    for (cases) |case| {
        var source = readFile(case.file, t.allocator);
        defer t.allocator.free(source);
        t.expectEqual(case.expt, try vm.interpret(&source)) catch |err| {
            std.debug.print("failed: {s}\n", .{case.file});
            return err;
        };
    }
}

test "garbage collection" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const result = try run.file("test/data/garbage.lox", vm);
    try t.expectEqual(True, result.?);
}

test "hash tables" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: str,
    };

    const cases = [_]Case{
        .{
            .expr =
            \\ var baz = #{};
            \\ baz.qux = "quux";
            \\ baz.foo = "foo";
            \\ return baz.qux + baz.foo;
            ,
            .expt = "quuxfoo",
        },
        .{
            .expr =
            \\ var qux = #{};
            \\ return tostring(qux["foo"]);
            ,
            .expt = "nil",
        },
        .{
            .expr =
            \\ var qux = #{};
            \\ qux.foo = "bar";
            \\ return qux["foo"];
            ,
            .expt = "bar",
        },
        .{
            .expr =
            \\ var qux = #{};
            \\ qux["foo"]= "baz";
            \\ return qux.foo;
            ,
            .expt = "baz",
        },
    };

    for (cases) |case| {
        const res = try vm.interpret(&case.expr);
        t.expectEqualSlices(
            u8,
            case.expt,
            res.as(*Obj).as(object.String).bytes,
        ) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "Arrays" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: str,
    };

    const cases = [_]Case{
        .{ .expr = "return tostring([1, 2, 3]);", .expt = "[1, 2, 3]" },
        .{ .expr = "return tostring(length([1, 2, 3]));", .expt = "3" },
        .{
            .expr =
            \\ var arr = [];
            \\ for (var i = 0; i < 3; i = i + 1)
            \\   arr[length(arr)] = length(arr);
            \\ return tostring(arr);
            ,
            .expt = "[0, 1, 2]",
        },
        .{
            .expr =
            \\ fun f(x) { return "hi " + tostring(x); }
            \\ var arr = [f];
            \\ return arr[0](10);
            ,
            .expt = "hi 10",
        },
        .{ .expr = "var a = []; return tostring(a == a);", .expt = "true" },
        .{ .expr = "var a = []; var b = []; return tostring(a == b);", .expt = "true" },
        .{ .expr = "return tostring([] == []);", .expt = "true" },
        .{ .expr = "return tostring([1] != []);", .expt = "true" },
        .{ .expr = "return tostring([1] != [1, 2]);", .expt = "true" },
        .{ .expr = "return tostring([1, [nil, [3]]] == [1, [nil, [3]]]);", .expt = "true" },
    };

    for (cases) |case| {
        const res = try vm.interpret(&case.expr);
        t.expectEqualSlices(
            u8,
            case.expt,
            res.as(*Obj).as(object.String).bytes,
        ) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}

test "hash sets" {
    var gc = GCAllocator.init(t.allocator);
    const allocator = gc.allocator();
    var vm = try VM.init(allocator);
    defer vm.deinit();

    const Case = struct {
        expr: str,
        expt: str,
    };

    const cases = [_]Case{
        .{
            .expr =
            \\ var baz = #[];
            \\ baz.qux = "quux";
            \\ baz.foo = "foo";
            \\ return baz.qux + baz.foo;
            ,
            .expt = "quxfoo",
        },
        .{
            .expr =
            \\ var qux = #[];
            \\ return tostring(qux["foo"]);
            ,
            .expt = "nil",
        },
        .{
            .expr =
            \\ var qux = #[];
            \\ qux.foo = "bar";
            \\ return qux["foo"];
            ,
            .expt = "foo",
        },
        .{
            .expr =
            \\ var qux = #[];
            \\ qux["foo"]= "baz";
            \\ return qux.foo;
            ,
            .expt = "foo",
        },
    };

    for (cases) |case| {
        const res = try vm.interpret(&case.expr);
        t.expectEqualSlices(
            u8,
            case.expt,
            res.as(*Obj).as(object.String).bytes,
        ) catch |err| {
            std.debug.print("failed: {s}\n", .{case.expr});
            return err;
        };
    }
}
